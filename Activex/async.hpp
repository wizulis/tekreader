#ifndef ASYNC_HPP
#define ASYNC_HPP
#include <QtPlugin>

class QWidget;

class Async{
public:
    virtual void Init(QWidget *)=0;
};

//#define Async_iid "Interface.Async"
//Q_DECLARE_INTERFACE(Async,Async_iid)
Q_DECLARE_METATYPE(Async*)

#endif // ASYNC_HPP

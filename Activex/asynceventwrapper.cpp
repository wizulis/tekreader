#include "asynceventwrapper.hpp"
#include "logger.hpp"
#include <thread>
#include <QDebug>

AsyncEventWrapper::AsyncEventWrapper()
{
    timer=nullptr;
}


AsyncEventWrapper & AsyncEventWrapper::getInstance(){

    static AsyncEventWrapper *inst = new AsyncEventWrapper();
    return *inst;
}

void AsyncEventWrapper::start(){
    if(timer)
        return;

    timer = new QTimer(this);
    connect(timer,&QTimer::timeout,this,&AsyncEventWrapper::CallAllEvents);
    timer->start(100);
}


void AsyncEventWrapper::WaitForAllEvents(int ms){


    qDebug() << "Waiting";
    std::lock_guard<std::mutex> lock(wait_mtx);

    /*  if(wait_mtx.lock())//std::chrono::milliseconds(ms<0? 1000: ms))){

       qDebug() << "Unlocking-Wait";
       wait_mtx.unlock();
   }
*/
   qDebug() << "Wait-Exit";
}

void AsyncEventWrapper::PushEvent(std::function<void ()> event){
    qDebug() << "Pushing event";
    std::lock_guard<std::mutex> lock(cmd_mtx);
    wait_mtx.try_lock();
    cmd_queue.push_back(event);
    qDebug() << "Event Pushed";

}

void AsyncEventWrapper::CallAllEvents(){


    //qDebug() << "Call Start";
    std::lock_guard<std::mutex> lock(cmd_mtx);
    while(!cmd_queue.empty()){
        try{

            qDebug() << "Exec Cmd";
         //   log() <<"Calling event\n";
            cmd_queue.front()();
        }catch(std::runtime_error & e){
            log() << "AsyncEvent: " << e.what() << "\n";
        }catch(...){
            log() << "AsyncEvent throwed - but not a runtimeerror \n";
        }

        cmd_queue.pop_front();
    }
    wait_mtx.try_lock();
    wait_mtx.unlock();

   // qDebug() << "Call End";
}

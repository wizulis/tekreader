#ifndef ASYNCEVENTWRAPPER_HPP
#define ASYNCEVENTWRAPPER_HPP

#include <QTimer>
#include <atomic>
#include <mutex>
#include <list>
#include <functional>
#include <QObject>

class AsyncEventWrapper: public QObject
{
    Q_OBJECT

    std::list<std::function<void(void)>> cmd_queue;
    std::mutex cmd_mtx;
    std::mutex wait_mtx;
    QTimer  *timer;

    AsyncEventWrapper();
    AsyncEventWrapper & operator=(const AsyncEventWrapper &);
    AsyncEventWrapper(const AsyncEventWrapper &);
public:

    static AsyncEventWrapper & getInstance();
    void start();

    void WaitForAllEvents(int ms=-1);
    void PushEvent(std::function<void(void)> event);
private slots:

    void CallAllEvents();

};

#endif // ASYNCEVENTWRAPPER_HPP

#include <functional>
#include <QDialog>
#include <QVBoxLayout>
#include "motorcontroler.hpp"
#include "logger.hpp"

MotorControler::MotorControler(QWidget *parent) :
   // QAxWidget(new QDialog(parent))
      QAxWidget(parent)
{
    auto * p = parentWidget();

    auto * lay= new QVBoxLayout(p);
    lay->addWidget(this);
    static_cast<QDialog*>(p)->setModal(false);
    static_cast<QDialog*>(p)->show();
    static_cast<QDialog*>(p)->resize(500,300);
    this->setControl("{3CE35BF3-1E13-4D2C-8C0B-DEF6314420B3}");

}

double MotorControler::getPosition(){
    QVariantList params;
    params << 0;
    return dynamicCall("GetPosition_Position(int)",params).toFloat();
}

void MotorControler::setSerial(int ser){
    QVariantList params;
    params << ser;
    dynamicCall("SetHWSerialNum(int)",params);
}

void MotorControler::setPosition(int chanel,double pos){

    QVariantList params;
    params << chanel << pos << pos << 1000 << false;
    dynamicCall("MoveAbsoluteEnc(int,double,double, int, bool)",params);
}

void MotorControler::startControl(){
    dynamicCall("StartCtrl()");
}

void MotorControler::home(){
    QVariantList params;
    params << 0 << 0;
    dynamicCall("MoveHome(int,int)",params);
}

MotorControler::~MotorControler(){
    dynamicCall("StopCtrl()");
    parentWidget()->deleteLater();
}

AsyncMotor::AsyncMotor(QObject * parent):QObject(parent){
    motor_obj=nullptr;
}

void AsyncMotor::Init(QWidget * par){
    motor_obj= new MotorControler(par);
}

void AsyncMotor::setSerial(int ser){
    QMetaObject::invokeMethod(motor_obj,"setSerial",Qt::BlockingQueuedConnection,Q_ARG(int,ser));
}

void AsyncMotor::setPosition(int chanel, double pos){
    QMetaObject::invokeMethod(motor_obj,"setPosition",Qt::BlockingQueuedConnection,Q_ARG(int,chanel),Q_ARG(double,pos));
}

void AsyncMotor::startControl(){
    QMetaObject::invokeMethod(motor_obj,"startControl",Qt::BlockingQueuedConnection);
}

AsyncMotor::~AsyncMotor(){
    if(!motor_obj){
        return;
    }

    QMetaObject::invokeMethod(motor_obj,"deleteLater",Qt::BlockingQueuedConnection);
}

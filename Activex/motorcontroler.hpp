#ifndef MOTORCONTROLER_HPP
#define MOTORCONTROLER_HPP

#include <QAxWidget>
#include "async.hpp"

class MotorControler : public QAxWidget
{
    Q_OBJECT

public:
    explicit MotorControler(QWidget *parent = 0);

    double getPosition();

    ~MotorControler();
signals:
    void MoveComplete(int);
    void HomeComplete(int);
public slots:
    void setSerial(int);

    void setPosition(int,double);
    void startControl();
    void home();
};


class AsyncMotor: public QObject,public Async{

    Q_OBJECT

    MotorControler * motor_obj;

public:
    AsyncMotor(QObject * parent=nullptr);

    void Init(QWidget * parent) override;
public slots:

    void setSerial(int);

    void setPosition(int , double);
    void startControl();
public:
    ~AsyncMotor();
};

#endif // MOTORCONTROLER_H

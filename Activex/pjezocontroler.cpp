#include <functional>
#include <QDialog>
#include <QVBoxLayout>
#include "pjezocontroler.h"
#include "logger.hpp"
#include <QDebug>
PjezoControler::PjezoControler(QWidget *parent) :
    QAxWidget(new QDialog(parent))
{
    auto * p = parentWidget();

    auto * lay= new QVBoxLayout(p);
    lay->addWidget(this);
    static_cast<QDialog*>(p)->setModal(false);
    static_cast<QDialog*>(p)->show();
    static_cast<QDialog*>(p)->resize(500,300);
    this->setControl("{A9BC6065-D4F4-4A77-B42B-215A9D6DF4C6}");

}


void PjezoControler::setSerial(int ser){
    QVariantList params;
    params << ser;
    dynamicCall("SetHWSerialNum(int)",params);
    params.clear();
    params << 0 << 2;
    dynamicCall("SetControlMode(int,int)",params);
    // setProperty("HWSerialNum",ser);
}

void PjezoControler::setPosition(int chanel,double pos){

    QVariantList params;
    params.push_back(chanel);
    params.push_back(pos);
    dynamicCall("SetPosOutput(int,double)",params);
}

void PjezoControler::startControl(){
    dynamicCall("StartCtrl()");
}


PjezoControler::~PjezoControler(){
   // parentWidget()->close();
    parentWidget()->deleteLater();

    qDebug() << "Closing Pjezo";
}

AsyncPjezo::AsyncPjezo(QObject * parent):QObject(parent){
    pjezo_obj=nullptr;
}

void AsyncPjezo::Init(QWidget * par){
    pjezo_obj= new PjezoControler(par);
}

void AsyncPjezo::setSerial(int ser){
    QMetaObject::invokeMethod(pjezo_obj,"setSerial",Qt::BlockingQueuedConnection,Q_ARG(int,ser));
}

void AsyncPjezo::setPosition(int chanel, double pos){
    QMetaObject::invokeMethod(pjezo_obj,"setPosition",Qt::BlockingQueuedConnection,Q_ARG(int,chanel),Q_ARG(double,pos));
}

void AsyncPjezo::startControl(){
    QMetaObject::invokeMethod(pjezo_obj,"startControl",Qt::BlockingQueuedConnection);
}

AsyncPjezo::~AsyncPjezo(){
    qDebug() << "Deleting";
    if(!pjezo_obj){
        qDebug() << "Returning";
        return;
    }

    qDebug() << "Calling delete";
    QMetaObject::invokeMethod(pjezo_obj,"deleteLater",Qt::BlockingQueuedConnection);
}

#ifndef PJEZOCONTROLER_H
#define PJEZOCONTROLER_H

#include <QAxWidget>
#include "asynceventwrapper.hpp"
#include "async.hpp"

class PjezoControler : public QAxWidget
{
    Q_OBJECT

public:
    explicit PjezoControler(QWidget *parent = 0);

    ~PjezoControler();
signals:

public slots:
    void setSerial(int);

    void setPosition(int,double);
    void startControl();
};


class AsyncPjezo: public QObject,public Async{

    Q_OBJECT

    //Q_INTERFACES(Async)
    PjezoControler * pjezo_obj;

public:
    AsyncPjezo(QObject * parent=nullptr);

    void Init(QWidget * parent) override;
public slots:

    void setSerial(int);

    void setPosition(int , double);
    void startControl();
public:
    ~AsyncPjezo();
};

#endif // PJEZOCONTROLER_H

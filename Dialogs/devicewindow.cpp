#include "devicewindow.hpp"

DeviceWindow::DeviceWindow(DeviceObj *parent) :
    QWidget(nullptr)
{
    connect(this,&QObject::destroyed,parent,&DeviceObj::closeDevice);
}

#ifndef DEVICEWINDOW_HPP
#define DEVICEWINDOW_HPP

#include <QWidget>
#include <QCloseEvent>
#include "deviceobj.hpp"

class DeviceWindow : public QWidget
{
    Q_OBJECT
public:
    explicit DeviceWindow(DeviceObj *parent = 0);

protected:
    virtual void closeEvent(QCloseEvent * event){

        event->accept();
        this->deleteLater();
    }

signals:

public slots:

};

#endif // DEVICEWINDOW_HPP

#include "osciloscopewindow.hpp"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>

#include <QDialog>
#include <QIntValidator>
#include <QDoubleValidator>
#include <QCheckBox>

#include <QMessageBox>
#include <QCoreApplication>
#include <QFileDialog>


OsciloscopeWindow * OsciloscopeWindow::last_loaded=nullptr;

OsciloscopeWindow::OsciloscopeWindow(QSharedPointer<VisaOsciloscope> &osc_obj, DeviceObj * parent) :
    DeviceWindow(parent),osc(osc_obj)
{
    last_loaded=this;

    resize(500,600);
    auto vbox1 = new QVBoxLayout(this);
    auto hbox1 = new QHBoxLayout();
    hbox1->addWidget(new QLabel(tr("Directory:"),this));
    dirInput=new QLineEdit(this);
    dirInput->setEnabled(false);
    dirInput->setText(QCoreApplication::applicationDirPath());
    hbox1->addWidget(dirInput);

    auto changeDir=new QPushButton(tr(".."),this);
    hbox1->addWidget(changeDir);
    vbox1->addLayout(hbox1);

    auto hboxfN = new QHBoxLayout();
    hboxfN->addWidget(new QLabel(tr("File name:"),this));
    fileInput=new QLineEdit(this);

    hboxfN->addWidget(fileInput);
    vbox1->addLayout(hboxfN);

    auto butBox = new QHBoxLayout();

    isRunning=osc->getRunStop();
    auto avg=osc->getAverage();
    isAverage=avg.first;
    avgCount=avg.second;

    runStop = new QPushButton( isRunning? "Stop": "Run" ,this);
    butBox->addWidget(runStop);

    average = new QPushButton(isAverage ? "Set Sample" : "Set Average" ,this);
    butBox->addWidget(average);

    auto expandCh = new QPushButton("Expand chanel",this);
    butBox->addWidget(expandCh);

    auto mesBut = new QPushButton("Mesure",this);
    butBox->addWidget(mesBut);
    auto saveBut = new QPushButton("Save",this);
    butBox->addWidget(saveBut);
    auto settingBut = new QPushButton("Settings",this);
    butBox->addWidget(settingBut);

    vbox1->addLayout(butBox);

    oscPlot= new OscPlot(osc,this);
    vbox1->addWidget(oscPlot,1);

    connect(changeDir,&QPushButton::clicked,this,&OsciloscopeWindow::ChangeDir);
    connect(runStop,&QPushButton::clicked,this,&OsciloscopeWindow::ToggleRunStop);
    connect(average,&QPushButton::clicked,this,&OsciloscopeWindow::ToggleAverage);
    connect(expandCh,&QPushButton::clicked,this,&OsciloscopeWindow::ExpandChanel);
    connect(mesBut,&QPushButton::clicked,this,&OsciloscopeWindow::Mesure);
    connect(settingBut,&QPushButton::clicked,this,&OsciloscopeWindow::EditSettings);
    connect(saveBut,&QPushButton::clicked,this,&OsciloscopeWindow::SaveData);

}

void OsciloscopeWindow::ChangeDir(){

    auto dir=QFileDialog::getExistingDirectory(this,tr("Izvēlies direktoriju"));
    dirInput->setText(dir);
}



void OsciloscopeWindow::SetRunStop(bool val){
    osc->setRunStop(val);
}

void OsciloscopeWindow::SaveDataNamed(const QString & name){
    auto res=osc->getLastData();
    if(res)
        res->saveToFile(name);
}
QString OsciloscopeWindow::GetSaveName(){
    return dirInput->text()+"/"+fileInput->text();
}

void OsciloscopeWindow::ExpandChanel(){

    QDialog dialog;

    dialog.setModal(true);
    auto * vbox= new QVBoxLayout();
    dialog.setLayout(vbox);


    QGridLayout * box = new QGridLayout();

    QComboBox * exp_chan= new QComboBox(&dialog);
    exp_chan->setEditable(false);
    QStringList chn;
    for(int i=1;i<=4;i++)
        chn << tr("%1").arg(i);
    exp_chan->addItems(chn);


    QLineEdit * exp_vDiv= new QLineEdit(&dialog);
    QLineEdit * exp_time= new QLineEdit(&dialog);
    QCheckBox * chkB = new QCheckBox(&dialog);
    chkB->setText("Two step");

    exp_vDiv->setValidator(new QDoubleValidator(0,10,3,&dialog));
    auto * tmp = new QIntValidator(&dialog);
    tmp->setBottom(1);
    exp_time->setValidator(tmp);


    box->addWidget(new QLabel("Chanel:",&dialog),0,0);
    box->addWidget(exp_chan,0,1);
    box->addWidget(new QLabel("V/div:",&dialog),1,0);
    box->addWidget(exp_vDiv,1,1);
    box->addWidget(new QLabel("Time(ms):",&dialog),2,0);
    box->addWidget(exp_time,2,1);
    box->addWidget(chkB,3,0,1,2,Qt::AlignRight);
    box->setColumnStretch(1,1);

    vbox->addLayout(box);
    auto * hbox= new QHBoxLayout();
    hbox->addStretch(1);
    auto * but_ok = new QPushButton(tr("Expand"),&dialog);
    auto * but_cancel = new QPushButton(tr("Cancel"),&dialog);

    connect(but_ok,&QPushButton::clicked,&dialog,&QDialog::accept);
    connect(but_cancel,&QPushButton::clicked,&dialog,&QDialog::close);

    hbox->addWidget(but_ok);
    hbox->addWidget(but_cancel);

    vbox->addLayout(hbox);

    if(QDialog::Accepted==dialog.exec()){
        osc->ExpandChanel(exp_chan->currentText().toInt(),exp_vDiv->text().toDouble(),exp_time->text().toInt(),chkB->isChecked());
    }
}

#include "logger.hpp"

void OsciloscopeWindow::Mesure(){
    if(selectedChanels.size()>0){
        try{
            osc->Mesure(QVector<int>::fromStdVector(selectedChanels));

            oscPlot->updatePlot();
        }catch(std::exception &e){
            QMessageBox::warning(this,"Mesure Failed",e.what(),QMessageBox::Ok);
        }
    }
}

void OsciloscopeWindow::ToggleRunStop(){
    try{
        osc->setRunStop(!isRunning);
        isRunning=!isRunning;
        if(isRunning){
            runStop->setText("Stop");
        }else{
            runStop->setText("Run");
        }

    }catch(std::exception &e){
        QMessageBox::warning(this,"Run/Stop failed",e.what(),QMessageBox::Ok);
    }

}
void OsciloscopeWindow::ToggleAverage(){
    try{
        osc->setAverage(!isAverage,avgCount);
        isAverage=!isAverage;

        if(!isAverage){
            //Setting to Sample mode
            average->setText("Set Average");
        }else{
            //Setting to Average mode;
            average->setText("Set Sample");
       }

    }catch(std::exception & e){
        QMessageBox::warning(this,"Set Average failed",e.what(),QMessageBox::Ok);
    }
}

void OsciloscopeWindow::EditSettings(){
    QDialog dialog;
    dialog.setModal(true);
    auto * vbox= new QVBoxLayout();
    dialog.setLayout(vbox);
    auto * grid = new QGridLayout();
    grid->addWidget(new QLabel(tr("Average count:"),&dialog),0,0);
    auto * avgC=  new QComboBox(&dialog);

    QStringList opts;

    for(int i=1;i<=10;i++){
        opts << tr("%1").arg(std::pow(2,i));
    }
    avgC->addItems(opts);
    avgC->setCurrentText(tr("%1").arg(avgCount));
    grid->addWidget(avgC,0,1);

    grid->addWidget(new QLabel(tr("Chanels:"),&dialog),1,0);

    QCheckBox * chn[4];
    for(int i=1;i<=4;i++){
        chn[i-1]= new QCheckBox(tr("CH%1").arg(i),&dialog);
        if(std::find(selectedChanels.begin(),selectedChanels.end(),i)==selectedChanels.end())
            chn[i-1]->setChecked(false);
        else
            chn[i-1]->setChecked(true);
        grid->addWidget(chn[i-1],i,1);
    }

    auto * hbox2 = new QHBoxLayout();
    auto ok_but = new QPushButton(tr("Ok"),&dialog);
    connect(ok_but,&QPushButton::clicked,&dialog,&QDialog::accept);
    auto cancel_but = new QPushButton(tr("Cancel"),&dialog);
    connect(cancel_but,&QPushButton::clicked,&dialog,&QDialog::close);
    hbox2->addWidget(ok_but);
    hbox2->addWidget(cancel_but);

    vbox->addLayout(grid);
    vbox->addLayout(hbox2);

    if(dialog.exec()==QDialog::Accepted){
        avgCount=avgC->currentText().toInt();
        osc->setAverage(isAverage,avgCount);

        selectedChanels.clear();
        for(int i=0;i<4;i++){
            if(chn[i]->isChecked()){
                selectedChanels.push_back(i+1);
            }
        }
    }
}

void OsciloscopeWindow::SaveData(){
    QString saveFile;

    if(fileInput->text()=="" ){
        saveFile=QFileDialog::getSaveFileName(this,tr("Save file"),dirInput->text(),"Text(*.txt)");
        if(!saveFile.endsWith(".txt")){
            saveFile+=".txt";
        }
    }else{
        saveFile=dirInput->text()+"/"+fileInput->text();
        if(QFileInfo(saveFile).exists()){
            if(QMessageBox::warning(this,"File Exists","File Exists, Overrite?",QMessageBox::Save | QMessageBox::Cancel)!=QMessageBox::Save){
                return;
            }
        }
    }
    auto res=osc->getLastData();
    if(res)
        res->saveToFile(saveFile);
}

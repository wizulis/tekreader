#ifndef OSCILOSCOPEWINDOW_HPP
#define OSCILOSCOPEWINDOW_HPP

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QComboBox>

#include "visaosciloscope.hpp"
#include "devicewindow.hpp"
#include "oscplot.hpp"


class OsciloscopeWindow : public DeviceWindow
{
    Q_OBJECT


    QLineEdit * dirInput,*fileInput;

    QPushButton *runStop, *average;

    QSharedPointer<VisaOsciloscope> osc;

    bool isRunning;
    bool isAverage;
    int avgCount;
    std::vector<int> selectedChanels;

    OscPlot * oscPlot;

    static OsciloscopeWindow * last_loaded;
public:

    static OsciloscopeWindow * getLastLoaded(){
        return last_loaded;
    }

    explicit OsciloscopeWindow(QSharedPointer<VisaOsciloscope> & osc_obj,DeviceObj * parent);

    ~OsciloscopeWindow(){
        if(last_loaded==this){
            last_loaded=nullptr;
        }
    }
    QSharedPointer<VisaOsciloscope> getOsc(){
        return osc;
    }

    void SaveDataNamed(const QString & name);

signals:

public slots:
    void SetRunStop(bool);
    QString GetSaveName();
    void ExpandChanel();
    void Mesure();
    void ToggleRunStop();
    void ToggleAverage();
    void EditSettings();
    void SaveData();
    void ChangeDir();
};

#endif // OSCILOSCOPEWINDOW_HPP

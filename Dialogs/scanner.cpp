#include "scanner.hpp"
#include <QDoubleValidator>
#include <QIntValidator>
#include <QVBoxLayout>
#include <QFileDialog>
#include <fstream>


const double Scanner::MINSTEP=0.001;
const double Scanner::MAXSTEP=2;

Scanner::Scanner(DeviceObj *parent) :
    DeviceWindow(parent)
{
    resize(600,600);
    obj= new Scanner2D(this);
    auto * vbox=new QVBoxLayout(this);
    vbox->addWidget(step= new QLineEdit(this));
    step->setValidator(new QDoubleValidator(MINSTEP,MAXSTEP,4,step));
    step->setText("0.2");
    vbox->addWidget(time = new QLineEdit(this));
    time->setValidator(new QIntValidator(MINSCANTIME,MAXSCANTIME,time));
    time->setText("30000");


    auto * hboxmag= new QHBoxLayout();
    hboxmag->addWidget(enableMag = new QCheckBox(this));
    hboxmag->addWidget(magSteps= new QLineEdit(this));
    vbox->addLayout(hboxmag);
    enableMag->setChecked(false);
    magSteps->setDisabled(true);

    connect(enableMag,&QCheckBox::stateChanged,
            [this](int state){
        if(state==2){
            auto filename=QFileDialog::getOpenFileName(this,"Select magnetic field file");
            magSteps->setText(filename);
        }
    });

    auto * hbox= new QHBoxLayout();
    hbox->addWidget(start= new QPushButton("Start",this));
    hbox->addWidget(spec_start= new QPushButton("Start and Save",this));
    vbox->addLayout(hbox);
    vbox->addWidget(obj);
    connect(start,&QPushButton::clicked,this,&Scanner::toggle);
    connect(spec_start,&QPushButton::clicked,this,&Scanner::toggle_spec);
    connect(step,&QLineEdit::editingFinished,this,&Scanner::updateStep);
    connect(time,&QLineEdit::editingFinished,this,&Scanner::updateTime);
}

void Scanner::updateStep(){
    obj->setStep(step->text().toDouble());
}

void Scanner::updateTime(){
    obj->setStepTime(time->text().toInt());
}

void Scanner::toggle(){

    if(obj->isActive()){
        obj->stop();
        start->setText("Start");
        spec_start->setText("Start and Save");

    }else{
        obj->start();
        start->setText("Stop");
        spec_start->setText("Stop");
    }
}

void Scanner::toggle_spec(){

    if(obj->isActive()){
        obj->stop();
        start->setText("Start");
        spec_start->setText("Start and Save");
    }else{
        std::vector<double> mag_steps;
        if(enableMag->isChecked()){
            std::fstream in;
            in.open(magSteps->text().toStdString(),std::ios::in);
            if(!in.is_open()){
                return;
            }
            double num;
            while(in>>num)
                mag_steps.push_back(num);
        }
        obj->start_spec(mag_steps);
        start->setText("Stop");
        spec_start->setText("Stop");
    }
}

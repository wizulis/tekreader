#ifndef SCANNER_HPP
#define SCANNER_HPP
#include "devicewindow.hpp"
#include <QLineEdit>
#include <QPushButton>
#include <QCheckBox>

#include "scanner2d.hpp"
#include "signalgen.hpp"

class Scanner : public DeviceWindow
{
    static const int MAXSCANTIME=1000*1000;//1000sec
    static const int MINSCANTIME=200;

    static const double MINSTEP;
    static const double MAXSTEP;

    Q_OBJECT
    QLineEdit *step;
    QLineEdit *time;
    QCheckBox * enableMag;
    QLineEdit * magSteps;

    QPushButton *start,*spec_start;
    Scanner2D *obj;
public:
    explicit Scanner(DeviceObj *parent = 0);

signals:

public slots:
    void updateStep();
    void updateTime();
    void toggle();
    void toggle_spec();
};

#endif // SCANNER_HPP

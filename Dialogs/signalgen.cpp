#include "signalgen.hpp"
#include <QVBoxLayout>
#include <QDoubleValidator>
#include <QPushButton>

SignalGen * SignalGen::cur_gen=nullptr;

SignalGen::SignalGen(DeviceObj *parent, QString params):DeviceWindow(parent),generator(params.toStdString()),isEnabled(true)
{
    auto vbox = new QVBoxLayout(this);
    vbox->addWidget(offset=new QLineEdit(this));

    offset->setValidator(new QDoubleValidator(-10,10,6,offset));

    vbox->addWidget(sendBut=  new QPushButton("Send",this));

    connect(sendBut,&QPushButton::clicked,this,&SignalGen::sendOffset);
    cur_gen=this;
}

void SignalGen::setDC(){
    generator.setDC();
}

void SignalGen::setOffset(double val){
    offset->setText(tr("%1").arg(val));
}

bool SignalGen::isDisabled(){
    return !isEnabled;
}

void SignalGen::disableUi(bool val){
    isEnabled=!val;
    offset->setReadOnly(val);
    sendBut->setDisabled(val);
}
void SignalGen::sendOffset(){
    generator.setOffset(offset->text().toDouble());
}


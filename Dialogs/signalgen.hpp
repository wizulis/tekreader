#ifndef SIGNALGEN_HPP
#define SIGNALGEN_HPP

#include "devicewindow.hpp"
#include "ttsignalgenerator.hpp"
#include <QLineEdit>

class SignalGen : public DeviceWindow
{

    Q_OBJECT
    TTSignalGenerator generator;
    bool isEnabled;
    QLineEdit * offset;
    QPushButton * sendBut;

    static SignalGen * cur_gen;
public:
    static SignalGen * getCurrentGenerator(){return cur_gen;}
    explicit SignalGen(DeviceObj * parent, QString params);
    void setDC();
    void setOffset(double val);
    void disableUi(bool disable);
    bool isDisabled();

    ~SignalGen(){
        if(cur_gen==this)
            cur_gen=nullptr;
    }

public slots:
    void sendOffset();
};

#endif // SIGNALGEN_HPP

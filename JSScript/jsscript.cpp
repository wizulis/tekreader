#include "jsscript.hpp"
#include <QFile>
#include <QTextStream>
#include <thread>
#include "logger.hpp"
#include "filewriter.hpp"
#include "Activex/pjezocontroler.h"
#include "Activex/motorcontroler.hpp"


Q_SCRIPT_DECLARE_QMETAOBJECT(FileWriter, QObject*)

QScriptValue logger_wrap ( QScriptContext * ctx, QScriptEngine * ) {
    log() << ctx->argument(0).toString().toStdString().c_str() << "\n";
    return QScriptValue();
}

QScriptValue toScriptValueQPR(QScriptEngine * eng,const QSharedPointer<Results> & ptr){
    QScriptValue obj=eng->newQObject((QObject*)ptr.data());
    return obj;
}

void fromScriptValueQPR(const QScriptValue &,QSharedPointer<Results> &){
    throw std::runtime_error("Unable to recreate shared ptr from script");
}

JSScript::JSScript(QWidget *parent) :
    QObject((QObject*)parent)
{

}


void JSScript::RunFile(const QString &FileName){
    QFile file(FileName);
    if(!file.open(QIODevice::ReadOnly|QIODevice::Text)){
        throw std::runtime_error("Unable to open script file");
    }

    QTextStream stream(&file);

    QString text=stream.readAll();

    RunScript(text,FileName);
}

void JSScript::RunScript(const QString &Script, const QString &fileName){
    if(thr.joinable())
        thr.join();
    thr=std::thread(&JSScript::Run,this,Script,fileName);
}

void JSScript::BindObject(const QString &name, QObject *obj){
    objs[name]=obj;
}


void JSScript::Run(const QString Script, const QString fileName){

    engine=QSharedPointer<QScriptEngine>(new QScriptEngine);
    qRegisterMetaType<Async*>("Async");
    qScriptRegisterMetaType(engine.data(),toScriptValueQPR,fromScriptValueQPR);
    qScriptRegisterSequenceMetaType<QVector<int>>(engine.data());
    qScriptRegisterSequenceMetaType<QVector<double>>(engine.data());
    qScriptRegisterSequenceMetaType<std::vector<double>>(engine.data());


    for(auto &i:objs){
        engine->globalObject().setProperty(i.first,engine->newQObject(i.second));
    }

    engine->globalObject().setProperty("Script",engine->newQObject(this));

    engine->globalObject().setProperty("print",engine->newFunction(logger_wrap));
    QScriptValue fileW = engine->scriptValueFromQMetaObject<FileWriter>();
    engine->globalObject().setProperty("FileWriter", fileW);

    if(!engine->canEvaluate(Script)){
        log() << "Unable to evaluate script";
        return;
    }
    try{
        engine->evaluate(Script,fileName);
    }
    catch(std::exception & e){
         log() << "Unable to finish script: " << e.what() << "\n";
    }
    catch(...)
    {
         log() << "Script failed with unknown error\n";
    }

    engine->collectGarbage();
    if(engine->hasUncaughtException()){
        log() << "Exception thrown: " << engine->uncaughtException().toString().toStdString() << "\n";
        engine.clear();
        return;
    }
    if(!engine.isNull())
        engine.clear();
}


JSScript::~JSScript(){
    if(thr.joinable())
        thr.join();
}

void JSScript::InitAsync(Async *ptr){
    ptr->Init(dynamic_cast<QWidget *>(parent()));
}

void JSScript::sleep(int ms){
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}
QScriptValue JSScript::CreatePjezo(){
    auto * ret = new AsyncPjezo();
    QMetaObject::invokeMethod(this,"InitAsync",Qt::BlockingQueuedConnection,Q_ARG(Async*,(Async*)ret));
    return engine->newQObject((QObject*)ret,QScriptEngine::ValueOwnership::ScriptOwnership);
}
QScriptValue JSScript::CreateMotor(){
    auto * ret = new AsyncMotor();
    QMetaObject::invokeMethod(this,"InitAsync",Qt::BlockingQueuedConnection,Q_ARG(Async*,(Async*)ret));
    return engine->newQObject((QObject*)ret,QScriptEngine::ValueOwnership::ScriptOwnership);
}



#ifndef JSSCRIPT_HPP
#define JSSCRIPT_HPP

#include <QObject>
#include <QScriptEngine>
#include <QString>
#include <thread>


#include "visaosciloscope.hpp"
class AsyncPjezo;
class AsyncMotor;
class Async;
class PythonScript;

class JSScript : public QObject
{
    Q_OBJECT


    QSharedPointer<QScriptEngine> engine;
    std::map<QString,QObject *> objs;
    std::thread thr;
public:
    explicit JSScript(QWidget *parent = 0);


    void RunFile( const QString & FileName);
    void RunScript( const QString & Script,const QString & fileName=QString());

    void BindObject(const QString & name, QObject * obj);

    ~JSScript();
signals:

private slots:
    void InitAsync(Async * ptr);
public slots:
    void Run(const QString  Script,const QString  fileName);

    void sleep(int ms);
    QScriptValue CreatePjezo();
    QScriptValue CreateMotor();

};

Q_DECLARE_METATYPE(QVector<int>)
Q_DECLARE_METATYPE(QVector<double>)
Q_DECLARE_METATYPE(std::vector<double>)
Q_DECLARE_METATYPE(QSharedPointer<Results>)

#endif // JSSCRIPT_HPP

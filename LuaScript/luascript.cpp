#include "luascript.hpp"
#include "luabind_shared_ptr_workaround.hpp"
#include "visaosciloscope.hpp"
#include "ttsignalgenerator.hpp"
#include "plotviewwidget.hpp"
#include "logger.hpp"
#include "Activex/pjezocontroler.h"
#include <thread>
#include<iostream>
#include <sstream>

void sleep(int ms){
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

int call_system(const std::string & cmd){
    return system(cmd.c_str());
}
void logFunc(const std::string & r){
    log() << r << "\n";
}

void WaitAllEvents(){
    AsyncEventWrapper::getInstance().WaitForAllEvents();
}

void setFunc(std::vector<double> * vec,const int ind,const double val){
    vec->at(ind)=val;
}

double getFunc(std::vector<double> * vec,const int ind){
    return vec->at(ind);
}
int vecSize(std::vector<double> * vec){
    return (int)vec->size();
}

void setFuncStr(std::vector<std::string> * vec,const int ind,const std::string &  val){
    vec->at(ind)=val;
}

std::string getFuncStr(std::vector<std::string> * vec,const int ind){
    return vec->at(ind);
}
int vecSizeStr(std::vector<std::string> * vec){
    return (int)vec->size();
}
std::shared_ptr<AsyncPjezo> CreatePjezo(PlotViewWidget * parent){
    auto ptr=std::shared_ptr<AsyncPjezo>(new AsyncPjezo(parent));
    WaitAllEvents();
    return ptr;
}

QSharedPointer<Results> Mesure(VisaOsciloscope * visa, std::vector<std::string> enums){
    QVector<CHANEL> chan;

    for(auto & i: enums){
        if(i=="CH1")
            chan.push_back(CHANEL::CH1);
        else if(i=="CH2")
                chan.push_back(CHANEL::CH2);
        else if(i=="CH3")
                chan.push_back(CHANEL::CH3);
        else if(i=="CH4")
                chan.push_back(CHANEL::CH4);
    }

    return visa->Mesure(chan);
}

std::shared_ptr<VisaOsciloscope> LuaScript::tek_ptr_global=std::shared_ptr<VisaOsciloscope>();


LuaScript::LuaScript(std::shared_ptr<VisaOsciloscope> & tek_ptr, PlotViewWidget *plotWidget):tek(tek_ptr),plotW(plotWidget)
{

}

void LuaScript::LoadObjects(lua_State * L,std::shared_ptr<VisaOsciloscope> & tek_ptr,PlotViewWidget * plotWidget){

    tek_ptr_global=tek_ptr;
    luabind::module(L)[
            luabind::def("sleep",sleep),
            luabind::def("log",logFunc),
            luabind::def("system",call_system),
            luabind::def("WaitAllEvents",WaitAllEvents)
            //luabind::def("Osc",getOsc)
            ];


    luabind::module(L)[
            luabind::class_<VisaOsciloscope,std::shared_ptr<VisaOsciloscope>>("VisaOsciloscope")
            .def("Mesure",Mesure)
            .def("RunStop",&VisaOsciloscope::setRunStop)
            .def("Average",&VisaOsciloscope::setAverage)
            .def("EnableTrig",&VisaOsciloscope::enableTriggerEvent)
            .def("DisableTrig",&VisaOsciloscope::disableTriggerEvent)
            .def("GetResults",&VisaOsciloscope::getLastData)
            ];

    luabind::module(L)[
            luabind::class_<Results,std::shared_ptr<Results> >("Results")
            .def("AddColumn",&Results::addColumn)
            .def("GetColumn",&Results::getColumn)
            .def("AddColumnOrder",&Results::addColumnOrder)
            .def("Save",&Results::saveToFile)

            ];

    luabind::module(L)[
            luabind::class_<std::vector<double> >("dVec")
            .def(luabind::constructor<int>())
            .def("Set",setFunc)
            .def("Get",getFunc)
            .def("Size",vecSize)
            ];

    luabind::module(L)[
            luabind::class_<std::vector<std::string> >("strVec")
            .def(luabind::constructor<int>())
            .def("Set",setFuncStr)
            .def("Get",getFuncStr)
            .def("Size",vecSizeStr)
            ];

    luabind::module(L)[
            luabind::class_<AsyncPjezo,std::shared_ptr<AsyncPjezo> >("Pjezo")
            .def("SetPosition",&AsyncPjezo::setPosition)
            .def("StartControl",&AsyncPjezo::startControl)
            .def("SetSerial",&AsyncPjezo::setSerial)];




    luabind::module(L)[
            luabind::class_<TTSignalGenerator>("TTSignalGen")
            .def(luabind::constructor<std::string>())
            .def("SetFreq",&TTSignalGenerator::setFrequency)
            .def("GetFreq",&TTSignalGenerator::getFrequency)
            ];
    if(plotWidget){

        luabind::module(L)[
                luabind::class_<PlotViewWidget >("PlotViewWidget")
                .def("Replot",&PlotViewWidget::replot)
                .def("SetXAxis",&PlotViewWidget::setXAxis)
                .def("GetFileName",&PlotViewWidget::getFileName)
                .def("EnableLines",&PlotViewWidget::enableLines)
                .def("CreatePjezoCntr",CreatePjezo)
                ];
        //luabind::globals(L)["Plot"]=plotWidget;
    }

}

void LuaScript::runScript(const std::string script){


    qDebug() << "Start Runscript";
    lua_State * L = luaL_newstate();
    luaL_openlibs(L);
    qDebug() << "Luabind open";
    luabind::open(L);
    qDebug() << "Luabind loadObjects";
    LoadObjects(L,tek,plotW);

    qDebug() << "Objects loaded";
    QFile file(":/luaFiles/luafunctions.lua");
    if(!file.open(QIODevice::ReadOnly)){
        log() << "Unable to open luafunction resource\n";
        return;
    }

    std::stringstream runScr;

    runScr << file.readAll().constData() << "\n\n" << script;



    int err=luaL_loadstring(L, runScr.str().c_str());

    if(err){
        log() << "Lua loading failed: " << err << "\n" << lua_tostring(L,-1)<< "\n";
        log() << "Script:\n" << runScr.str() << "\n";
        lua_close(L);
        return;
    }
    qDebug() << "Calling lua";
    try{
        err=lua_pcall(L,0,LUA_MULTRET,0);
        if(err)
            throw std::exception("Error on first");
        luabind::globals(L)["Osc"]=tek;
       // luaL_loadstring(L,"Osc:Mesure({})");
       // err=lua_pcall(L,0,LUA_MULTRET,0);


    }catch(std::exception & e){
        qDebug() << "Fail: " << e.what();
    }

    if(err){
        log() << "Lua run failed: " << err << "\n" << lua_tostring(L,-1)<< "\n";
        lua_close(L);
        return;
    }

    lua_close(L);

}

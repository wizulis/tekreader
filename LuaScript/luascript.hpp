#ifndef LUASCRIPT_HPP
#define LUASCRIPT_HPP

#ifndef Q_MOC_RUN
#pragma warning(push)
#pragma warning(disable: 4251)
#include <luabind/luabind.hpp>
#pragma warning(pop)
#endif

extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

class VisaOsciloscope;
class PlotViewWidget;
class LuaScript
{
    std::shared_ptr<VisaOsciloscope> tek;
    PlotViewWidget * plotW;

public:

    static std::shared_ptr<VisaOsciloscope> tek_ptr_global;
    LuaScript(std::shared_ptr<VisaOsciloscope> & tek_ptr,PlotViewWidget * plotWidget=nullptr);
    void runScript(const std::string scriptText);
    void LoadObjects(lua_State *,std::shared_ptr<VisaOsciloscope>&,PlotViewWidget *);
};

#endif // LUASCRIPT_HPP

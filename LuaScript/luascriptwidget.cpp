#include "luascriptwidget.hpp"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QTextStream>
#include <QFileDialog>
#include <thread>
#include <fstream>

LuaScriptWidget::LuaScriptWidget(std::shared_ptr<VisaOsciloscope> tek_ptr,PlotViewWidget * plot,QWidget *parent) :
    QWidget(parent)
{
    jscr.BindObject("Plot",(QObject*)(plot));
    jscr.BindObject("Osc",(QObject*)tek_ptr.get());
    auto vbox=new QVBoxLayout(this);

    auto hbox1=new QHBoxLayout();

    auto loadScript=new QPushButton(tr("Ielādēt skriptu"),this);
    hbox1->addWidget(loadScript);
    auto saveScript=new QPushButton(tr("Saglabāt izmaiņas"),this);
    hbox1->addWidget(saveScript);
    auto saveAsScript=new QPushButton(tr("Saglabāt kā .."),this);
    hbox1->addWidget(saveAsScript);
    hbox1->addStretch(1);

    auto runScript=new QPushButton(tr("Palaist skriptu"),this);
    hbox1->addWidget(runScript);

    vbox->addLayout(hbox1);
    text=new QTextEdit(this);
    vbox->addWidget(text,1);
    text->zoomIn(2);
    connect(loadScript,&QPushButton::clicked,this,&LuaScriptWidget::loadScript);
    connect(saveScript,&QPushButton::clicked,this,&LuaScriptWidget::saveScript);
    connect(saveAsScript,&QPushButton::clicked,this,&LuaScriptWidget::saveAsScript);
    connect(runScript,&QPushButton::clicked,this,&LuaScriptWidget::runScript);
}
#include <iostream>
void LuaScriptWidget::loadScript(){
    QString fileName=QFileDialog::getOpenFileName(this,tr("Izvēlieties skriptu"),tr(""),tr("JavaScript (*.js)"));

    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly|QIODevice::Text))
        return;

    QTextStream in(&file);
    QString tx=in.readAll();
    text->setText(tx);
    scriptFile=QFileInfo(fileName);

}

void LuaScriptWidget::saveScript(){
    if(scriptFile.isWritable() || !scriptFile.exists()){
        std::ofstream out;
        out.open(scriptFile.absoluteFilePath().toStdString(),std::ios::trunc);
        if(!out.is_open())
            return;

        out << text->document()->toPlainText().toStdString();
    }
}
void LuaScriptWidget::saveAsScript(){
    QString fName=QFileDialog::getSaveFileName(this,"Save JavaScript",QString(),tr("JS files (*.js)"));
    scriptFile=QFileInfo(fName);
    saveScript();
}

void LuaScriptWidget::runScript(){
    QString scriptText=text->toPlainText();

    jscr.RunScript(scriptText);
}

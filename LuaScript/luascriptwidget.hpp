#ifndef LUASCRIPTWIDGET_HPP
#define LUASCRIPTWIDGET_HPP

#include <QFileInfo>
#include <QWidget>
#include <QTextEdit>
#include <thread>
#include "JSScript/jsscript.hpp"

class PlotViewWidget;

class LuaScriptWidget : public QWidget
{
    Q_OBJECT

    QTextEdit * text;
    std::thread scriptThread;
    JSScript jscr;

    QFileInfo scriptFile;
public:
    explicit LuaScriptWidget(std::shared_ptr<VisaOsciloscope> tek_ptr,PlotViewWidget * plot,QWidget *parent = 0);

    ~LuaScriptWidget(){
        if(scriptThread.joinable())
            scriptThread.join();
    }
signals:

public slots:
    void loadScript();
    void saveScript();
    void saveAsScript();
    void runScript();
};

#endif // LUASCRIPTWIDGET_HPP

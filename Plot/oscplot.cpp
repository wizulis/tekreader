#include "oscplot.hpp"
#include <QVBoxLayout>
#include <QGridLayout>

OscPlot::OscPlot(QSharedPointer<VisaOsciloscope> & p_osc,QWidget *parent) :
    QWidget(parent),osc(p_osc),h_curs(nullptr),v_curs(nullptr)
{
    auto * vbox=new QVBoxLayout(this);
    plot = new QCustomPlot(this);
    plot->setBackground(QBrush(Qt::black));

    h0_line=new QCPItemLine(plot);
    h0_line->setPen(QPen(Qt::white,2,Qt::DashLine));

    h0_line->start->setCoords(0,0.5);
    h0_line->end->setCoords(QCPRange::maxRange,0.5);

    v0_line=new QCPItemLine(plot);
    v0_line->setPen(QPen(Qt::white,2,Qt::DashLine));
    v0_line->start->setCoords(plot->xAxis->range().center(),0);
    v0_line->end->setCoords(plot->xAxis->range().center(),1);

    plot->xAxis->setNumberPrecision(3);

    plot->yAxis->setRange(0.0,1.0);
    plot->yAxis->setTickLabels(false);
    plot->yAxis2->setRange(0.0,1.0);
    plot->yAxis2->setTickLabels(false);
    plot->xAxis2->setTickLabels(false);

    plot->xAxis->setAutoTickStep(false);
    plot->yAxis->setAutoTickStep(false);
    plot->xAxis2->setAutoTickStep(false);
    plot->yAxis2->setAutoTickStep(false);

    plot->yAxis->setTickStep(0.1);
    plot->yAxis2->setTickStep(0.1);

    auto colorF=[](QCPAxis * ax){
        ax->setBasePen(QPen(Qt::white));
        ax->setTickLabelColor(Qt::white);
        ax->setTickPen(QPen(Qt::white));
        ax->setSubTickPen(QPen(Qt::white));
        ax->setVisible(true);
    };

    colorF(plot->xAxis);
    colorF(plot->yAxis);
    colorF(plot->xAxis2);
    colorF(plot->yAxis2);
    vbox->addWidget(plot,1);

    QColor colors[]={Qt::yellow,Qt::green,Qt::magenta,Qt::cyan};
    for(int i=0;i<4;i++){
        auto graph=plot->addGraph();
        graph->setPen(QPen(colors[i]));
    }
    auto * gridbox = new QGridLayout();

    for(int i=0;i<5;i++)//Time,Ch1-4
        coords[i] = new QLabel(this);
    QStringList labels;
    labels << "Time:" << "CH1:" << "CH2:" << "CH3:" << "CH4:";
    for(int i=0;i<5;i++){
        gridbox->addWidget(new QLabel(labels[i],this),0,2*i);
        gridbox->addWidget(coords[i],0,2*i+1);
    }
    gridbox->setColumnStretch(10,1);
    vbox->addLayout(gridbox);

    connect(plot,&QCustomPlot::mouseMove,this,&OscPlot::updateCoordinates);
    connect(plot,&QCustomPlot::mouseRelease,this,&OscPlot::toogleCursor);
}

void OscPlot::updateCoordinates(QMouseEvent *  event){
    double x,y;
    plot->graph(0)->pixelsToCoords(event->posF(),x,y);
    coords[0]->setText(tr("%1%2").arg(x).arg(time_unit));

    for(int i=0;i<4;i++){
        double val=(y-0.5)*vDivs[i]*10+offsets[i]-positions[i]*vDivs[i];
        QString unit="V";
        if(std::abs(val)<1 && std::abs(val)>1e-3){
            val*=1e3;
            unit="mV";
        }else if(std::abs(val)<=1e-3 && std::abs(val)>1e-6 ){
            val*=1e6;
            unit="mkV";
        }

        coords[i+1]->setText(tr("%1%2").arg(val,0,'g',5).arg(unit) );
    }
}
void OscPlot::toogleCursor(QMouseEvent *  event){
    if(event->button()==Qt::LeftButton){
        double x,y;
        x=plot->xAxis->pixelToCoord(event->pos().x());
        y=plot->yAxis->pixelToCoord(event->pos().y());
        if(h_curs)
            plot->removeItem(h_curs);
        if(v_curs)
            plot->removeItem(v_curs);

        h_curs= new QCPItemLine(plot);
        plot->addItem(h_curs);
        h_curs->setPen(QPen(Qt::white,1,Qt::DashLine));
        h_curs->start->setCoords(0,y);
        h_curs->end->setCoords(QCPRange::maxRange,y);

        v_curs= new QCPItemLine(plot);
        plot->addItem(v_curs);
        v_curs->setPen(QPen(Qt::white,1,Qt::DashLine));
        v_curs->start->setCoords(x,0);
        v_curs->end->setCoords(x,1.0);
        plot->replot();
    }else if(event->button()==Qt::RightButton){
        if(h_curs)
            plot->removeItem(h_curs);
        if(v_curs)
            plot->removeItem(v_curs);
        h_curs=nullptr;
        v_curs=nullptr;
        plot->replot();
    }
}
#include "logger.hpp"
void OscPlot::updatePlot(){

    updateParams();
    auto data=osc->getLastData();
    auto x=QVector<double>::fromStdVector(data->mesurments["TIME"]);


    if(x.last()<1 && x.last()>1e-3){
        time_unit="ms";
        std::transform(x.begin(),x.end(),x.begin(),[](double x){return x*1e3;});
    }else if(x.last()<=1e-3 && x.last()>1e-6){
        time_unit="mks";
        std::transform(x.begin(),x.end(),x.begin(),[](double x){return x*1e6;});
    }else if(x.last()<=1e-6 && x.last()>1e-9){
        time_unit="ns";
        std::transform(x.begin(),x.end(),x.begin(),[](double x){return x*1e9;});
    }

    for(int i=0;i<4;i++){

        auto ptr=data->mesurments.find(tr("CH%1").arg(i+1));
        if(data->mesurments.end()==ptr){
            plot->graph(i)->clearData();
        }
        else{
            auto y=*ptr;
            std::transform(y.begin(),y.end(),y.begin(),[&](double sig){return (sig-offsets[i]+positions[i]*vDivs[i])/vDivs[i]/10.0+0.5;});
            plot->graph(i)->setData(x,QVector<double>::fromStdVector(y));
        }
    }



    plot->xAxis->setRange(0.0,x.last());
    plot->xAxis->setTickStep(x.last()/10.0);
    plot->xAxis2->setRange(0.0,x.last());
    plot->xAxis2->setTickStep(x.last()/10.0);

    v0_line->start->setCoords(plot->xAxis->range().center(),0);
    v0_line->end->setCoords(plot->xAxis->range().center(),1);
    plot->replot();
}

void OscPlot::updateParams(){
    for(int i=0;i<4;i++){
        vDivs[i]=osc->GetVDivision(i+1);
        offsets[i]=osc->GetVOffset(i+1);
        positions[i]=osc->GetPosition(i+1);

    }
}

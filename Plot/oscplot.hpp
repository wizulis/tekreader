#ifndef OSCPLOT_HPP
#define OSCPLOT_HPP

#include <QWidget>
#include <QLabel>

#include "visaosciloscope.hpp"
#include "qcustomplot.h"

class OscPlot : public QWidget
{
    Q_OBJECT
    QSharedPointer<VisaOsciloscope> osc;

    double offsets[4];
    double vDivs[4];
    double positions[4];

    QLabel * coords[5];
    QCustomPlot * plot;
    QCPItemLine * h_curs,*v_curs,*h0_line,*v0_line;

    QString time_unit;

public:
    explicit OscPlot(QSharedPointer<VisaOsciloscope> & p_osc,QWidget * parent=0);

signals:

public slots:
    void updatePlot();
    void updateParams();
    void updateCoordinates(QMouseEvent *  event);
    void toogleCursor(QMouseEvent * event);
};

#endif // OSCPLOT_HPP

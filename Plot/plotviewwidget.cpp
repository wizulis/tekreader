#include "plotviewwidget.hpp"
#include "mainwindow.hpp"
#include "logger.hpp"
#include "indicatorlight.hpp"
#include <QPushButton>
#include <QStatusBar>
#include <QMessageBox>
#include <QDir>
#include <QFileInfo>

PlotViewWidget::PlotViewWidget(std::weak_ptr<VisaOsciloscope> tek_ptr,MainWindow *parent) :
    QWidget(parent),tek(tek_ptr),parentWindow(parent),xAxisName("TIME"),linesFlag(true),scriptWidget(nullptr)
{
    auto * vbox1 = new QVBoxLayout(this);

    auto * hbox2= new QHBoxLayout();

    for(int i=0;i<4;i++){
        CHbox[i]=new QCheckBox(tr("CH%1").arg(i+1),this);
        connect(CHbox[i],&QCheckBox::clicked,this,&PlotViewWidget::showPlots);
        hbox2->addWidget(CHbox[i]);
    }

    hbox2->addStretch(1);
    avgbox=new QCheckBox(tr("Avg"),this);
    hbox2->addWidget(avgbox);

    hbox2->addStretch(1);

    runStopLight=new IndicatorLight([](int st){
        switch(st)
        {
        case 0:
            return QColor(255,0,0);
        case 1:
            return QColor(0,255,0);
        default:
            return QColor(0,0,0);
        }
    },this);
    hbox2->addWidget(runStopLight);

    auto * runStop=new QPushButton(tr("Run/Stop"),this);
    hbox2->addWidget(runStop);

    auto getData=new QPushButton(tr("Ielādēt datus"),this);
    hbox2->addWidget(getData);

    auto save=new QPushButton(tr("Saglabāt"),this);
    hbox2->addWidget(save);
    auto runScript=new QPushButton(tr("Palaist skriptu"),this);
    hbox2->addWidget(runScript);

    vbox1->addLayout(hbox2);

    plotLayout=new QGridLayout();
    QColor colors[4]={Qt::yellow,Qt::blue,QColor(233,0,255)/*Purple?*/,Qt::green};

    for(int i=0;i<4;i++){
        CHPlots.push_back(new QCustomPlot(this));
        QCustomPlot * plot=CHPlots.last();
        plot->plotLayout()->insertRow(0);
        auto title=new QCPPlotTitle(plot,QString::fromStdString(std::to_string(i+1)));
        plot->plotLayout()->addElement(0,0,title);

        plot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
        plot->axisRect()->setRangeZoom(Qt::Vertical);
        plot->axisRect()->setRangeDrag(Qt::Vertical);
        plot->addGraph();
        plot->graph(0)->setPen(QPen(colors[i]));
    }

    QWidget * proxy=new QWidget(this);
    vbox1->addWidget(proxy,1);

    proxy->setLayout(plotLayout);

    connect(avgbox,&QCheckBox::clicked,this,&PlotViewWidget::setAverage);
    connect(runStop,&QPushButton::clicked,this,&PlotViewWidget::runStop);

    connect(save,&QPushButton::clicked,this,&PlotViewWidget::save);
    connect(getData,&QPushButton::clicked,this,&PlotViewWidget::loadData);
    connect(runScript,&QPushButton::clicked,this,&PlotViewWidget::runScript);
    for(int i=0;i<4;i++)
        CHbox[i]->setChecked(true);
    avgbox->setChecked(false);
    running=false;

    std::shared_ptr<VisaOsciloscope> x=tek.lock();
    if(x){
        QPair<bool,int> val=x->getAverage();
        if(val.first)
            avgbox->setChecked(true);
        if(x->getRunStop())
            running=true;
    }
    runStopLight->setState(running?1:0);
    showPlots();
}


QString PlotViewWidget::getFileName()const{
    QDir dir(parentWindow->getDirectory());
    QString fName(parentWindow->getFileName());
    return dir.absoluteFilePath(fName);
}

void PlotViewWidget::runStop(){
    auto tek_ptr=tek.lock();
    if(!tek_ptr)
        return;
    running=!running;
    tek_ptr->setRunStop(running);
    runStopLight->setState(running?1:0);
}

void PlotViewWidget::setAverage(){
    auto tek_ptr=tek.lock();
    if(!tek_ptr)
        return;
    tek_ptr->setAverage(avgbox->isChecked(),128);
}

void PlotViewWidget::save(){

    auto tek_ptr=tek.lock();
    if(!tek_ptr)
    {
        parentWindow->statusBar()->showMessage(tr("Ierīce nav sasniedzama!"));
        QMessageBox box;
        box.setText(tr("Ierīce nav sasniedzama!"));
        box.exec();
        return;
    }

    try{
        if(QFileInfo(getFileName()).exists()){

              if( QMessageBox::question(this, "Fails jau eksistē", "Vai Tu tiešām vēlies pārrakstīt failu?",QMessageBox::Yes|QMessageBox::No)==QMessageBox::No){
                  return;
              }
        }

        tek_ptr->getLastData()->saveToFile(getFileName());
    }
    catch(std::exception & e){
        parentWindow->statusBar()->showMessage(tr("Neizdevās saglabāt failu"));
        QMessageBox box;
        box.setText(tr("Neizdevās saglabāt failu:\n")+e.what());
        box.exec();
        return;
    }
}
void PlotViewWidget::loadData(){
    auto tek_ptr=tek.lock();

    if(!tek_ptr){
        parentWindow->statusBar()->showMessage(tr("Ierīce nav sasniedzama!"));
        QMessageBox box;
        box.setText(tr("Ierīce nav sasniedzama!"));
        box.exec();
        return;
    }

    QVector<int> vec;

    for(int i=0;i<4;i++){
        if(CHbox[i]->isChecked())
            vec.push_back(i+1);
    }

    try{

        parentWindow->statusBar()->showMessage(tr("Tiek veikta mērīšana"));
        tek_ptr->Mesure(vec);
        parentWindow->statusBar()->showMessage(tr("Gatavs"));
        replot();
        }
    catch(std::exception & e){
        QMessageBox box;
        box.setText(tr("Neizdevās nomērīt: ")+QString::fromStdString(e.what()));
        box.exec();
    }
}

void PlotViewWidget::replot(){

    if(xAxisName.size()==0)
        return;

    auto tek_ptr=tek.lock();
    if(!tek_ptr)
        return;

   auto data=tek_ptr->getLastData();

   auto itr=data->mesurments.find(xAxisName);
   if(itr==data->mesurments.end()){
       return;
   }
   std::vector<double> xAx=*itr;

   auto PlotF=[&](int a){
       auto graph=CHPlots[a-1]->graph(0);
       if(linesFlag){
           graph->setLineStyle(QCPGraph::lsLine);
        }
       else{
           graph->setLineStyle(QCPGraph::lsNone);
           QCPScatterStyle style;
           style.setSize(5);
           style.setShape(QCPScatterStyle::ssCircle);
           style.setPen(graph->pen());
           style.setBrush(graph->brush());
           graph->setScatterStyle(style);

       }
       auto itr=data->mesurments.find(tr("CH%1").arg(a));
       if(itr!=data->mesurments.end()){
            graph->setData(QVector<double>::fromStdVector(xAx),QVector<double>::fromStdVector(*itr) );
            graph->rescaleAxes();
            CHPlots[a-1]->replot();
        }
   };
   for(int i=1;i<=4;i++)
       PlotF(i);
}


void PlotViewWidget::enableLines(bool flag){
    linesFlag=flag;
}

void PlotViewWidget::showPlots(){

    int curIndex=0;
    for(int i=0;i<4;i++){
        if(!CHbox[i]->isChecked()){
            CHPlots[i]->hide();
            continue;
    }

        CHPlots[i]->show();
        plotLayout->addWidget(CHPlots[i],curIndex/2,curIndex%2);
        curIndex++;
    }
}

void PlotViewWidget::setXAxis(const QString & ax){
    xAxisName=ax;
}

void PlotViewWidget::runScript(){
    if(scriptWidget)
        scriptWidget->runScript();
}

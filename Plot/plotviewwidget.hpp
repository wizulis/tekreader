#ifndef PLOTVIEWWIDGET_HPP
#define PLOTVIEWWIDGET_HPP

#include <memory>
#include <QWidget>
#include <QVector>
#include <QCheckBox>
#include <QGridLayout>
#include "qcustomplot.h"
#include "TekInterface.hpp"
#include "indicatorlight.hpp"
#include "luascriptwidget.hpp"

class MainWindow;

class PlotViewWidget : public QWidget
{
    Q_OBJECT

    QCheckBox * CHbox[4];
    QVector<QCustomPlot*> CHPlots;
    QGridLayout * plotLayout;
    QCheckBox * avgbox;
    IndicatorLight * runStopLight;
    MainWindow * parentWindow;
    std::weak_ptr<VisaOsciloscope> tek;
    QString xAxisName;
    bool linesFlag;
    bool running;
    LuaScriptWidget * scriptWidget;
public:
    explicit PlotViewWidget(std::weak_ptr<VisaOsciloscope> tek_ptr,MainWindow *parent = 0);

    void setScript(LuaScriptWidget * scr){
        scriptWidget=scr;
    }

signals:

public slots:
    QString getFileName()const;
    void setAverage();
    void runStop();
    void save();
    void loadData();
    void replot();
    void showPlots();
    void setXAxis(const QString & );
    void enableLines(bool flag);
    void runScript();
};

#endif // PLOTVIEWWIDGET_HPP

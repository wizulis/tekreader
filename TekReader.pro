#-------------------------------------------------
#
# Project created by QtCreator 2013-09-16T15:19:06
#
#-------------------------------------------------

QT= core widgets printsupport help
TARGET = TekReader
#CONFIG   += console
#CONFIG   += app_bundle
QT += axcontainer script

TEMPLATE = app


SOURCES += main.cpp \
    Visa/TekInterface.cpp \
    mainwindow.cpp \
    Plot/qcustomplot.cpp \
   # Plot/plotviewwidget.cpp \
   # LuaScript/luascriptwidget.cpp \
    Visa/yakosciloscope.cpp \
    Visa/testosciloscope.cpp \
    logview.cpp \
  #  helpwindow.cpp \
    Visa/agilentosciloscope.cpp \
    Visa/visaosciloscope.cpp \
    Visa/visainterface.cpp \
    Visa/ttsignalgenerator.cpp \
  #  positionsocket.cpp \
   # Activex/pjezocontroler.cpp \
   # Activex/asynceventwrapper.cpp\
   # JSScript/jsscript.cpp \
   # filewriter.cpp \
    Activex/motorcontroler.cpp \
    Dialogs/osciloscopewindow.cpp \
    Dialogs/devicewindow.cpp \
    deviceobj.cpp \
    Plot/oscplot.cpp \
    scanner2d.cpp \
    Dialogs/scanner.cpp \
    Dialogs/signalgen.cpp


HEADERS += Visa/TekInterface.hpp \
    mainwindow.hpp \
    Plot/qcustomplot.h \
    #Plot/plotviewwidget.hpp \
    #LuaScript/luascriptwidget.hpp \
    Visa/yakosciloscope.hpp \
    Visa/testosciloscope.hpp \
    logger.hpp \
    #LuaScript/luabind_shared_ptr_workaround.hpp \
    #indicatorlight.hpp \
    logview.hpp \
    #helpwindow.hpp \
    Visa/agilentosciloscope.hpp \
    Visa/visaosciloscope.hpp \
    Visa/visainterface.hpp \
    Visa/ttsignalgenerator.hpp \
   # positionsocket.hpp \
   # Activex/pjezocontroler.h \
   # Activex/asynceventwrapper.hpp \
   # JSScript/jsscript.hpp \
   # filewriter.hpp \
   # Activex/async.hpp \
    Activex/motorcontroler.hpp \
    Dialogs/osciloscopewindow.hpp \
    Dialogs/devicewindow.hpp \
    deviceobj.hpp \
    Plot/oscplot.hpp \
    scanner2d.hpp \
    Dialogs/scanner.hpp \
    Dialogs/signalgen.hpp

INCLUDEPATH+=  . Activex\ Visa\ Plot\ LuaScript\
INCLUDEPATH+= "C:\boost_1_55_0"

contains(QMAKE_HOST.arch, x86_64):{
    INCLUDEPATH+= "C:\Program Files\IVI Foundation\VISA\Win64\Include" \
                    "C:\Python27\include"

    LIBS+= /LIBPATH:"C:\Program Files\IVI Foundation\VISA\Win64\Lib_x64\msc" \
           /LIBPATH:"C:\Python27\libs" \
           /LIBPATH:"C:\boost_1_55_0\stage\lib64"
    LIBS+=  -lvisa64
}else{

    INCLUDEPATH+= "C:\Program Files (x86)\IVI Foundation\VISA\WinNT\include" \
                  "C:\Python27_32\include"

    LIBS+= /LIBPATH:"C:\Program Files (x86)\IVI Foundation\VISA\WinNT\lib\msc" \
           /LIBPATH:"C:\Python27_32\libs" \
           /LIBPATH:"C:\boost_1_55_0\stage\lib32"
    LIBS+= -lvisa32
}


OTHER_FILES += \
    tekreader.qdocconf \
    tekreader.qhp \
    Doc/luainterface.qdoc


QMAKE_RESOURCE_FLAGS += -compress 0


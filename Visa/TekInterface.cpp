#include "TekInterface.hpp"
#include "logger.hpp"
#include <sstream>
#include <thread>
#include <numeric>
#include <QDebug>

TekOsciloscope::TekOsciloscope(const std::string &deviceName):VisaOsciloscope(deviceName){

    viSetAttribute(vi,VI_ATTR_WR_BUF_OPER_MODE,VI_FLUSH_ON_ACCESS);
    viSetAttribute(vi,VI_ATTR_RD_BUF_OPER_MODE,VI_FLUSH_ON_ACCESS);
    sendComand("HEADER OFF\n");
}

QString TekOsciloscope::chToString(const int ch) const{
    if(ch<0 || ch>4)
        throw std::runtime_error("TekOscilsocope::chToString: unknown chanel");
    else
        return tr("CH%1").arg(ch);
}

void TekOsciloscope::selectChanel(int chan){
    std::stringstream cmd;
    cmd << ":DATA:SOURCE " << chToString(chan).toStdString() << ";";
    cmd << ":DATA:ENCDG SRIBINARY;";
    cmd << ":DATA:WIDTH 2;";
    cmd << ":DATA:START 1;STOP 2500;\n";
    sendComand(cmd.str());
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

}

std::tuple<long,float,float,float> TekOsciloscope::getChanelParams(){
    std::tuple<long,float,float,float> ret;

    std::get<0>(ret)=readValue<long>("HOR:RECO?\n");

    std::get<1>(ret)=readValue<float>("WFMPRE:YOFF?\n");
    std::get<2>(ret)=readValue<float>("WFMPRE:YMULT?\n");
    std::get<3>(ret)=readValue<float>("WFMPRE:YZERO?\n");

    return ret;
}

std::vector<double> TekOsciloscope::getCurve(std::tuple<long, float, float, float> &params){
    viSetAttribute(vi,VI_ATTR_RD_BUF_OPER_MODE,VI_FLUSH_DISABLE);

    sendComand("CURVE?\n");

    viFlush(vi,VI_WRITE_BUF|VI_READ_BUF_DISCARD);

    ViStatus status;
    ViUInt32 length;
    char c;
    // The check char, Curve response is in the form:
    // #<0-9>{number of fields depending on previous value}{Data}
    // and we cannot skip so we read it

    status=viRead(vi,(ViBuf)&c,1,&length);
    checkStatus(status,"Failed reading #");
    if(c!='#')
        throw std::runtime_error("Incorrect response, expected #");




    status=viRead(vi,(ViBuf)&c,1,&length);
    checkStatus(status,"Failed to scan start number size");
    if(c<'0' || c>'9')
        throw std::runtime_error("Incorrect start number size");

    //Read in data length;
    int count=c-'0';
    std::string len;
    for(int i=0;i<count;i++){
        status=viRead(vi,(ViBuf)&c,1,&length);
        len+=c;
        checkStatus(status,"Size reading failed");
    }

    std::stringstream conv(len);
    int dataL;
    conv >> dataL;
    if(dataL==0)
        throw std::runtime_error("Record size 0!");

    //Get actual data
    std::vector<short> resultData(dataL/2);
    status=viRead(vi,(ViBuf)&resultData.front(),dataL,&length);
    checkStatus(status,"Curve data reading failed");

    log() << "Read " << std::to_string(length) << " bytes\n";
    std::vector<double> mesResults(dataL/2);
    for(std::size_t i=0;i<mesResults.size();i++){
       mesResults[i]=(((double)resultData[i])-std::get<1>(params))*std::get<2>(params)+std::get<3>(params);
    }
    viSetAttribute(vi,VI_ATTR_RD_BUF_OPER_MODE,VI_FLUSH_ON_ACCESS);

    return mesResults;
}

double TekOsciloscope::getTimeStep(){
    return readValue<float>("WFMPRE:XINCR?\n");
}

void TekOsciloscope::setAverage(bool flag, int avg){
    std::stringstream cmd;

    if(flag){
        cmd << ":ACQUIRE:MODE AVERAGE;:ACQUIRE:NUMAVG " << avg <<";\n";
    }
    else{
        cmd << ":ACQUIRE:MODE SAMPLE;\n";
    }

    sendComand(cmd.str());
}

QPair<bool,int> TekOsciloscope::getAverage(){
    bool isAvg=readValue<std::string>(":ACQUIRE:MODE?\n")=="AVERAGE";

    int num = readValue<int>(":ACQUIRE:NUMAVG?\n");

    return qMakePair(isAvg,num);
}

bool TekOsciloscope::getRunStop(){
    return readValue<int>(":ACQUIRE:STATE?")==1;
}

void TekOsciloscope::setRunStop(bool flag){
    std::stringstream cmd;
    cmd << "ACQUIRE:STATE " << flag? 1:0;
    cmd << "\n";

    sendComand(cmd.str());
}


void TekOsciloscope::SetTriggerChanel(int chan){

    std::stringstream cmd;
    cmd << "Trigger:Main:Edge:Source CH" << chan << "\n";
    sendComand(cmd.str());
}

void TekOsciloscope::SetTimeDiv(double tdiv){

    std::stringstream cmd;
    cmd << "HORIZONTAL:MAIN:SCALE " << tdiv << "\n";
    sendComand(cmd.str());
}
void TekOsciloscope::SetVOffset(int ,double){
    throw std::runtime_error("Osciloscope does not support offset, use position insted");
}

void TekOsciloscope::SetVPosition(int chan, double pos){
    sendComand("CH"+std::to_string(chan)+":POSITION "+std::to_string(pos)+"\n");
}

void TekOsciloscope::SetVDivision(int chan ,double div){
    std::stringstream cmd;
    cmd << "CH" << chan << ":SCALE " << div << "\n";
    sendComand(cmd.str());
}

double TekOsciloscope::GetVDivision(int chan){
    return readValue<float>("CH"+std::to_string(chan)+":SCALE?");
}

double TekOsciloscope::GetPosition(int chan){
    return readValue<float>("CH"+std::to_string(chan)+":POSITION?");
}

double TekOsciloscope::GetVOffset(int){
    return 0;
}

void TekOsciloscope::ExpandChanel(int chan, double div, int ms, bool twoStep){

    try{
        selectChanel(chan);
        setAverage(true,128);
        SetVPosition(chan,0);
        SetVDivision(chan,1.0);
        setRunStop(true);
        std::this_thread::sleep_for(std::chrono::milliseconds(ms));
        setRunStop(false);

        auto curve=getCurve(getChanelParams());
        if(curve.size()==0){
            throw std::runtime_error("Unable to get curve, specify longer sleep time or check if chanel is on");
        }
        auto avg = std::accumulate(curve.begin(),curve.end(),0.0);
        avg=avg/curve.size();

//        std::this_thread::sleep_for(std::chrono::milliseconds(200));


        if(twoStep){
            SetVDivision(chan,0.100);
            SetVPosition(chan,-avg/0.1);
            setRunStop(true);
            std::this_thread::sleep_for(std::chrono::milliseconds(ms));
            setRunStop(false);
            curve=getCurve(getChanelParams());
            avg = std::accumulate(curve.begin(),curve.end(),0.0);
            avg=avg/curve.size();
        }

        SetVDivision(chan,div);
        SetVPosition(chan,-avg/div);
        setRunStop(true);
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    catch(std::exception & e){
        log() << "Expand chanel failed " << e.what() << "\n";
    }

}

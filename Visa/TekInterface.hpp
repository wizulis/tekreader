#pragma once

#include "visaosciloscope.hpp"

class TekOsciloscope: public VisaOsciloscope{

public:
    TekOsciloscope(const std::string & deviceName);

    virtual QString chToString(const int ch) const override;
    virtual void selectChanel(int chan) override;
    virtual std::tuple<long,float,float,float> getChanelParams() override;
    virtual std::vector<double> getCurve(std::tuple<long,float,float,float> & params) override;
    virtual double getTimeStep() override;
    virtual void setAverage(bool flag,int avg) override;
    virtual QPair<bool,int> getAverage() override;
    virtual bool getRunStop() override;
    virtual void setRunStop(bool flag)override;

    virtual void SetTriggerChanel(int chan) override;
    virtual void SetTimeDiv(double timeDiv) override;
    virtual void SetVOffset(int chan,double offset) override;
    virtual void SetVPosition(int chan, double pos) override;
    virtual void SetVDivision(int chan,double div) override;
    virtual double GetVDivision(int chan) override;
    virtual double GetVOffset(int chan) override;
    virtual double GetPosition(int) override;


    virtual void ExpandChanel(int chan, double div, int ms, bool twoStep) override;
};

#include "agilentosciloscope.hpp"
#include <sstream>
#include <cstdlib>
#include <QFile>
#include <thread>

#include "logger.hpp"
#include <QTextStream>


AgilentOsciloscope::AgilentOsciloscope(const std::string &device):VisaOsciloscope(device){


    sendComand(":WAVeform:FORMat WORD");
    sendComand(":Waveform:points:mode MAX");
    sendComand(":Waveform:points 20000");
    sendComand(":Waveform:unsigned 0");
    sendComand(":Waveform:byteorder lsbfirst");

}


double AgilentOsciloscope::GetPosition(int ch){
    return 0;
}

double AgilentOsciloscope::GetVOffset(int ch){
   auto val= readValue<float>(":"+chToString(ch).toStdString()+":OFFSET?" );

   return val;
}

double AgilentOsciloscope::GetVDivision(int ch){
    auto val= readValue<float>(":"+chToString(ch).toStdString()+":SCALE?" );
    return val;
}

QString AgilentOsciloscope::chToString(const int ch)const  {
    switch(ch){
    case 1:
        return "CHAN1";
    case 2:
        return "CHAN2";
    case 3:
        return "CHAN3";
    case 4:
        return "CHAN4";
    }
    throw std::runtime_error("Unknonw chanel");
}
void AgilentOsciloscope::selectChanel(int chan) {



    sendComand(":WAVEFORM:SOURCE "+chToString(chan).toStdString());

    //sendComand(":Digitize "+chToString(chan).toStdString());

}

std::tuple<long,float,float,float> AgilentOsciloscope::getChanelParams() {




    long dataLength=readValue<long>(":WAV:Points?");
    float range=readValue<float>(":WAV:YINCREMENT?");
    float offset=readValue<float>(":WAV:YORigin?");
    float ref=readValue<float>(":WAV:YREFerence?");

    return std::make_tuple(dataLength,range,offset,ref);
}

std::vector<double> AgilentOsciloscope::getCurve(std::tuple<long,float,float,float> & params)  {
    if(std::get<0>(params)<0)
        return std::vector<double>();

    sendComand(":WAV:DATA?\n");
    ViStatus status;
    ViUInt32 length;

    char c;
    status=viRead(vi,(ViBuf)&c,1,&length);
    checkStatus(status,"Failed reading #");
    if(c!='#')
        throw std::runtime_error("Inccorrect response, expected #");

    status=viRead(vi,(ViBuf)&c,1,&length);
    checkStatus(status,"Failed to scan start number size");
    if(c<'0' || c>'9')
        throw std::runtime_error("Incorrect start number size");

    //Read in data length;
    int count=c-'0';
    std::string len;
    for(int i=0;i<count;i++){
        status=viRead(vi,(ViBuf)&c,1,&length);
        len+=c;
        checkStatus(status,"Size reading failed");
    }

    std::stringstream conv(len);
    int dataL;
    conv >> dataL;
    if(dataL==0)
        throw std::runtime_error("Record size 0!");

    //Get actual data

    std::vector<short> resultData(dataL/2,0);
    status=viRead(vi,(ViBuf)resultData.data(),dataL,&length);
    checkStatus(status,"Curve data reading failed");
    status=viRead(vi,(ViBuf)&c,1,&length);

    std::vector<double> mesResults(dataL/2);
    for(std::size_t i=0;i<mesResults.size();i++){
       double val=(double)resultData[i];
       mesResults[i]=std::get<1>(params)*(val-std::get<3>(params))+std::get<2>(params);
    }

    //std::this_thread::sleep_for(std::chrono::milliseconds(100));
    return mesResults;

}

double AgilentOsciloscope::getTimeStep() {
    return 1/readValue<float>(":ACQuire:SRATe?");
}

void AgilentOsciloscope::setAverage(bool flag,int avg)  {
    if(flag){
        sendComand(":ACQ:TYPE AVER\n");
        sendComand(":ACQ:COUNT "+std::to_string(avg));
    }else{
        sendComand(":ACQ:TYPE NORM\n");
    }
}

QPair<bool,int> AgilentOsciloscope::getAverage()  {
    bool isOn=readValue<std::string>(":ACQUIRE:MODE?\n")!="NORM\n";
    int avgNum=readValue<int>(":ACQUIRE:COUNT?\n");
    return qMakePair(isOn,avgNum);
}

bool AgilentOsciloscope::getRunStop() {
    return 0!=readValue<short>("TER?");
}

void AgilentOsciloscope::setRunStop(bool flag)  {
    if(flag){
        sendComand(":RUN");
        std::vector<int> chh(4);
        for(int i=1;i<=4;i++){
            chh[i-1]=readValue<int>(":"+chToString(i).toStdString()+":DISPLAY?");
            std::cout << "disp " << chh[i-1] << std::endl;
        }

        std::stringstream str;
        str << ":DIGITIZE ";
        bool comma=false;
        for(int i=0;i<4;i++){

            if(chh[i]){
                if(comma)
                    str <<",";
                str << "CHAN" << i+1;
                comma=true;
            }
        }
        sendComand(str.str());
    }else{
        sendComand(":STOP");
    }
}


QSharedPointer<Results>  AgilentOsciloscope::Mesure(QVector<int>  ch){
    std::vector<int> chh(4);
    for(int i=1;i<=4;i++){
        chh[i-1]=readValue<int>(":"+chToString(i).toStdString()+":DISPLAY?");
        std::cout << "disp " << chh[i-1] << std::endl;
    }
    auto ret=VisaOsciloscope::Mesure(ch);

    for(int i=1;i<=4;i++){
        sendComand(":"+chToString(i).toStdString()+":DISPLAY "+std::to_string(chh[i-1]));
    }

    return ret;
}

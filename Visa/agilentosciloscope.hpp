#ifndef AGILENTOSCILOSCOPE_HPP
#define AGILENTOSCILOSCOPE_HPP
#include "visaosciloscope.hpp"
class AgilentOsciloscope : public VisaOsciloscope
{
    Q_OBJECT
public:


    virtual double GetPosition(int);
    virtual double GetVOffset(int);
    virtual double GetVDivision(int);

    AgilentOsciloscope(const std::string & device);
    virtual QString chToString(const int ch)const override;
    virtual void selectChanel(int chan)override;
    virtual std::tuple<long,float,float,float> getChanelParams()override;
    virtual std::vector<double> getCurve(std::tuple<long,float,float,float> & params)override;
    virtual double getTimeStep()override;
    virtual void setAverage(bool flag,int avg)override;
    virtual QPair<bool,int> getAverage()override;
    virtual bool getRunStop()override;
    virtual void setRunStop(bool flag) override;
    QSharedPointer<Results> Mesure(QVector<int>  ch);
};

#endif // AGILENTOSCILOSCOPE_HPP

#include "testosciloscope.hpp"
#include <algorithm>
#include <functional>

TestOsciloscope::TestOsciloscope():VisaOsciloscope()
{
}

QString TestOsciloscope::chToString(const int ch) const{
    return tr("CH%1").arg(ch);
}

void TestOsciloscope::selectChanel(int chan){
    selChan=chan;
}

std::tuple<long,float,float,float> TestOsciloscope::getChanelParams(){
    return std::make_tuple((long)1000,1.0f,1.0f,1.0f);
}

std::vector<double> TestOsciloscope::getCurve(std::tuple<long, float, float, float> &){

    std::vector<double> ret(1000);
    int place=0;

    std::function<double()> func;
    switch (selChan) {
    case 1:
        func=[&place](){return sin(place++/180.0);};break;
    case 2:
        func=[&place](){return pow(sin(place++/180.0),2);};break;
    case 3:
        func=[&place](){return exp(-place++/400.0);};break;
    case 4:
        func=[&place](){return place>200 && place<600? 0:1;};break;
    }

    std::generate(ret.begin(),ret.end(),func);

    return ret;
}

double TestOsciloscope::getTimeStep(){
    return 0.05;
}

void TestOsciloscope::setAverage(bool flag, int avg){
    isAvg=flag;
    AvgC=avg;
}
QPair<bool,int> TestOsciloscope::getAverage(){
   return qMakePair(isAvg,AvgC);
}

bool TestOsciloscope::getRunStop(){
    return isRun;
}

void TestOsciloscope::setRunStop(bool flag){
    isRun=flag;
}

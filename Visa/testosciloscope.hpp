#ifndef TESTOSCILOSCOPE_HPP
#define TESTOSCILOSCOPE_HPP
#include "visaosciloscope.hpp"

class TestOsciloscope : public VisaOsciloscope
{

    bool isAvg;
    bool isRun;
    int AvgC;
    int selChan;
public:
    TestOsciloscope();

    virtual QString chToString(const int ch) const override;
    virtual void selectChanel(int chan) override;
    virtual std::tuple<long,float,float,float> getChanelParams() override;
    virtual std::vector<double> getCurve(std::tuple<long,float,float,float> & params) override;
    virtual double getTimeStep() override;
    virtual void setAverage(bool flag,int avg) override;
    virtual QPair<bool,int> getAverage() override;
    virtual bool getRunStop() override;
    virtual void setRunStop(bool flag)override;
};

#endif // TESTOSCILOSCOPE_HPP

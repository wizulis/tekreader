#include "ttsignalgenerator.hpp"
#include "logger.hpp"

TTSignalGenerator::TTSignalGenerator(const std::string &deviceName):VisaInterface(deviceName){
}

void TTSignalGenerator::setFrequency(const double freq){
    sendComand("FREQ "+std::to_string(freq)+"\n","Frequency not set!");
}


void TTSignalGenerator::setWFM(const QString & name){
    sendComand("WAVE "+name.toStdString()+"\n");
}

void TTSignalGenerator::setAmplitude(double val){
    sendComand("AMPL " + std::to_string(val)+"\n");
}
void TTSignalGenerator::setLOLVL(double val){
     sendComand("LOLVL " + std::to_string(val)+"\n");
}
void TTSignalGenerator::setHILVL(double val){
     sendComand("HILVL " + std::to_string(val)+"\n");
}
void TTSignalGenerator::setDC(){
    sendComand("ARBLOAD DC\n");
}

void TTSignalGenerator::setOffset(double volts){
    sendComand("ARBDCOFFS "+std::to_string(volts));

}

void TTSignalGenerator::setOutput(bool on){
    if(on)
        sendComand("OUTPUT ON\n");
    else
        sendComand("OUTPUT OFF\n");
}

TTSignalGenerator::~TTSignalGenerator(){
}

#ifndef TTSIGNALGENERATOR_HPP
#define TTSIGNALGENERATOR_HPP

#include "visainterface.hpp"
#include <QObject>
#include <sstream>

class TTSignalGenerator : public QObject, public VisaInterface
{
    Q_OBJECT
public:
    TTSignalGenerator(const std::string & deviceName);

public slots:

    void setWFM(const QString &);
    void setLOLVL(double val);
    void setHILVL(double val);
    void setAmplitude(double);
    void setOutput(bool);
    void setFrequency(const double freq);
    void setDC();
    void setOffset(double volts);
public:
    virtual ~TTSignalGenerator();

};

#endif // TTSIGNALGENERATOR_HPP

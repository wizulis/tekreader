#include "visainterface.hpp"

#include <istream>
#include <sstream>
#include "logger.hpp"
#include <thread>
#include <QDebug>

VisaInterface::VisaInterface(const std::string & deviceName):vi(0){
    qDebug() << "Loading " << deviceName.c_str() ;
    VISACOM::getInstance().OpenConnection();
    if(viOpen(VISACOM::getInstance().rm,(ViRsrc)deviceName.c_str(),VI_NULL,VI_NULL,&vi)!=VI_SUCCESS)
        throw std::runtime_error("Unable to open connection to the device");
}



template<typename T>
T VisaInterface::readValue(const std::string & command)const {

    T ret;
    //ViUInt32 length;
    //auto status=viWrite(vi,(ViBuf)command.c_str(),(ViUInt32)command.size(),&length);
    //checkStatus(status,);
    sendComand(command,std::string("Failed writing ")+command);
    char buffer[100];
    ViUInt32 bytesRead;

    auto status=viRead(vi,(ViBuf)buffer,100,&bytesRead);
    checkStatus(status,std::string("Failed reading ")+command );

    buffer[bytesRead]='\0';
    std::istringstream iss(buffer);
    iss >> ret ;
    return ret;

}

template  char VisaInterface::readValue(const std::string & command)const;
template  short VisaInterface::readValue(const std::string & command)const;
template  int VisaInterface::readValue(const std::string & command)const;
template  long VisaInterface::readValue(const std::string & command)const;
template  float VisaInterface::readValue(const std::string & command)const;
//template  double VisaInterface::readValue(const std::string & command)const;

template<> std::string VisaInterface::readValue(const std::string & command)const{
    sendComand(command,std::string("Failed writing ")+command);

    char buffer[1000];
    ViUInt32 bytesRead;
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    auto status=viRead(vi,(ViBuf)buffer,1000,&bytesRead);
    checkStatus(status,std::string("Failed reading ")+command );
    if(bytesRead==1000)
        throw std::logic_error(command + " response too long - needs fixing!");
    buffer[bytesRead]='\0';

    return std::string(buffer);

}


void VisaInterface::checkStatus(ViStatus status,const std::string & errMessage){
    if(status>=VI_SUCCESS)
        return;

    throw std::runtime_error(errMessage+std::string(" Error Code: ")+errToString(status));
}

void VisaInterface::sendComand(const std::string &cmd,const std::string & errMsg)const{
    ViUInt32 var;
    std::cout << "Sending " << cmd << std::endl;
    checkStatus(viWrite(vi,(ViBuf)cmd.c_str(),(ViUInt32)cmd.size(),&var),errMsg);

   // std::this_thread::sleep_for(std::chrono::milliseconds(100));
}

std::string VisaInterface::errToString(decltype(VI_SUCCESS) errr){

    switch(errr){
    case VI_ERROR_INV_SESSION:
        return "INVALID SESSION";
    case VI_ERROR_RSRC_LOCKED:
        return "RESOURCE LOCKED";
    case VI_ERROR_IO:
        return "IO ERROR";
    case VI_ERROR_TMO:
        return "TIMEOUT ERROR";
    case VI_ERROR_INV_FMT:
        return "WRITE FMT OR READ FMT INVALID";
    case VI_ERROR_NSUP_FMT:
        return "FORMAT NOT SUPPORTED";
    case VI_ERROR_ALLOC:
        return "COULD NOT ALLOCATE FORMATED IO BUFFER";
    }

    return std::to_string((long)errr);
}




VisaInterface::~VisaInterface(void){
    if(vi)
        viClose(vi);
}

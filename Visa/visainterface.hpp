#ifndef VISAINTERFACE_HPP
#define VISAINTERFACE_HPP

#include "visa.h"
#include <string>

class VisaInterface
{


    struct VISACOM{

        VISACOM():rm(0){}

        void OpenConnection(){
            if(rm==0)
                if(viOpenDefaultRM(&rm)!=VI_SUCCESS)
                    throw std::runtime_error("Unable to open RM");
        }

        static VISACOM & getInstance(){
            static VISACOM obj;
            return obj;
        }

        ~VISACOM(){
            if(rm!=0)
                viClose(rm);
        }

        ViSession rm;

    };


    VisaInterface(const VisaInterface & );
//    VisaInterface& operator = (VisaOsciloscope && );
protected:
    ViSession vi;
    template<class T> T readValue(const std::string & command)const;
    void sendComand(const std::string & cmd,const std::string & errMsg=std::string())const;
    VisaInterface():vi(0){}
public:
    VisaInterface(const std::string & deviceName);

    virtual ~VisaInterface();
    static std::string errToString(decltype(VI_SUCCESS) errr);
    static void checkStatus(ViStatus status, const std::string & errMessage);

};

#endif // VISAINTERFACE_HPP

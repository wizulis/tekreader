#include "visaosciloscope.hpp"

#include <fstream>
#include <iostream>
#include <thread>
#include <sstream>

#include <numeric>
#include "logger.hpp"
#include "testosciloscope.hpp"
#include "yakosciloscope.hpp"
#include "TekInterface.hpp"
#include "agilentosciloscope.hpp"


ViStatus _VI_FUNCH  TriggerHandler(ViSession,ViEventType,ViEvent,ViAddr){

    //std::cout << "Trigger\n";
    return VI_SUCCESS;
}

VisaOsciloscope::VisaOsciloscope(const std::string &deviceName):VisaInterface(deviceName){}

std::shared_ptr<VisaOsciloscope> VisaOsciloscope::getObject(const std::string &type, const std::string &device){
    if(device=="TEST"){
        return std::shared_ptr<VisaOsciloscope>(new TestOsciloscope());
    }else if(type=="Yokogawa"){
        return std::shared_ptr<VisaOsciloscope>( new YakOsciloscope(device));
    }else if(type=="Tektronix"){
        return std::shared_ptr<VisaOsciloscope>(new TekOsciloscope(device));
    }else if(type=="Agilent"){
        return std::shared_ptr<VisaOsciloscope>(new AgilentOsciloscope(device));
    }
    return std::shared_ptr<VisaOsciloscope>();
}



void VisaOsciloscope::enableTriggerEvent(){
    auto status=viInstallHandler(vi,VI_EVENT_TRIG,TriggerHandler,NULL);
    if(status < VI_SUCCESS)
        std::cout << "Failed install " << status << std::endl;

    if(viEnableEvent(vi,VI_EVENT_TRIG,VI_HNDLR,VI_NULL)< VI_SUCCESS)
        std::cout << "Failed enable" << std::endl;
}

void VisaOsciloscope::disableTriggerEvent(){
    viDisableEvent(vi,VI_EVENT_TRIG,VI_HNDLR);
}


QSharedPointer<Results>  VisaOsciloscope::Mesure(QVector<int>  ch){
    lastMesurementResults=QSharedPointer<Results>(new Results());
    Results & ret = *lastMesurementResults;

    std::size_t recordSize=0;
    for(const int chan:ch){
        log() << "Mesuring " << tr("CH%1").arg(chan).toStdString() << "\n";
        selectChanel(chan);
        auto params=getChanelParams();
        ret.mesurments[tr("CH%1").arg(chan)]=getCurve(params);
        if(ret.mesurments[tr("CH%1").arg(chan)].size()>recordSize)
            recordSize=(int)ret.mesurments[tr("CH%1").arg(chan)].size();
   }
    std::size_t maxSize=0;
    for(auto & i:ret.mesurments)
        if(i.size()>maxSize)
            maxSize=(int)i.size();
    std::vector<double> time(maxSize);

    double timeStep=getTimeStep();
    for(std::size_t i=0;i<maxSize;i++)
        time[i]=i*timeStep;
    ret.mesurments["TIME"]=std::move(time);

    return lastMesurementResults;
}
void VisaOsciloscope::SetTriggerChanel(int){ throw std::runtime_error("SetTriggerChanel - NotSupported");}
void VisaOsciloscope::SetTimeDiv(double){ throw std::runtime_error("SetTimeDiv - NotSupported");}
void VisaOsciloscope::SetVOffset(int,double){ throw std::runtime_error("SetVOffset - NotSupported");}
void VisaOsciloscope::SetVPosition(int,double){ throw std::runtime_error("SetVPosition - NotSupported");}
void VisaOsciloscope::SetVDivision(int,double){ throw std::runtime_error("SetVDivision - NotSupported");}

double VisaOsciloscope::GetPosition(int){ throw std::runtime_error("GetVPosition - NotSupported");}
double VisaOsciloscope::GetVOffset(int){ throw std::runtime_error("GetVOffset - NotSupported");}
double VisaOsciloscope::GetVDivision(int){ throw std::runtime_error("GetVDivision - NotSupported");}

void VisaOsciloscope::ExpandChanel(int,double,int,bool){ throw std::runtime_error("ExpandChanel - NotSupported");}

QSharedPointer<Results>  VisaOsciloscope::getLastData()const{
    return lastMesurementResults;
}

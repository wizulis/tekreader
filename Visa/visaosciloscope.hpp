#ifndef VISAOSCILOSCOPE_HPP
#define VISAOSCILOSCOPE_HPP


#include <vector>
#include <map>
#include <string>
#include <tuple>
#include <fstream>
#include <memory>
#include "visainterface.hpp"
#include <QObject>
#include <QPair>
#include <QVector>
#include <QSharedPointer>
#include <QMap>



struct Results: public QObject{

    Q_OBJECT
public:

    QMap<QString,std::vector<double> > mesurments;
    QMap<int,QString> colOrder;

public slots:

    void addColumn(const QString & name,std::vector<double> & data){
        mesurments[name]=data;
    }

    std::vector<double> getColumn(const QString & name){
        return mesurments[name];
    }

    void addColumnOrder(int ord,const QString & name){
        colOrder[ord]=name;
    }

    void saveToFile(const QString & fileName){
        std::ofstream out(fileName.toStdString(),std::ios::out | std::ios::trunc);
        if(!out.is_open())
            throw std::runtime_error("Unable to open file");

        if(mesurments.size()==0)
            return;
        int maxSize=0;
        for(auto & i:mesurments)
            if((int)i.size()>maxSize)
                maxSize=(int)i.size();

        QVector<QString> ord;
        //Map is sorted container
        for(auto &i:colOrder)
            ord.push_back(i);

        for(auto & i:mesurments.keys()){
            bool found=false;

            for(auto & x:ord){
                if(x==i){
                    found=true;
                    break;
                }    
            }
            if(!found)
                ord.push_back(i);
        }

        for(int i=0;i<ord.size();i++){
            if(i!=0)
                out << "\t";
            out << ord[i].toStdString();
        }
        out << "\n";

        for(int i=0;i<maxSize;i++){
            for(int j=0;j<ord.size();j++){
                if(j!=0)
                    out << "\t";
                auto &tmpRef=*mesurments.find(ord[j]);
                if((int)tmpRef.size()>i)
                    out << tmpRef[i];
            }
            out << "\n";
        }

    }
};


class VisaOsciloscope:public QObject, public VisaInterface {

    Q_OBJECT
protected:
    QSharedPointer<Results> lastMesurementResults;
    VisaOsciloscope():VisaInterface(){}
public:

    static std::shared_ptr<VisaOsciloscope> getObject(const std::string &type,const std::string &device);

    VisaOsciloscope(const std::string & deviceName);

public slots:
    virtual void enableTriggerEvent();
    virtual void disableTriggerEvent();
    virtual QString chToString(const int ch)const =0;
    virtual void selectChanel(int chan)=0;
    virtual std::tuple<long,float,float,float> getChanelParams()=0;
    virtual std::vector<double> getCurve(std::tuple<long,float,float,float> & params)=0;
    virtual double getTimeStep()=0;
    virtual void setAverage(bool flag,int avg)=0;
    virtual QPair<bool,int> getAverage()=0;
    virtual bool getRunStop()=0;
    virtual void setRunStop(bool flag)=0;
    virtual QSharedPointer<Results>  Mesure(QVector<int>  ch);
    virtual void SetTriggerChanel(int chan);
    virtual void SetTimeDiv(double timeDiv);

    virtual void SetVPosition(int chan,double pos);
    virtual void SetVOffset(int chan,double offset);
    virtual void SetVDivision(int chan,double div);

    virtual double GetPosition(int);
    virtual double GetVOffset(int);
    virtual double GetVDivision(int);

    virtual void ExpandChanel(int chan,double div,int ms,bool twoStep);


    QSharedPointer<Results> getLastData()const;
};

#endif // VISAOSCILOSCOPE_HPP

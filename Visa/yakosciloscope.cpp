#include "yakosciloscope.hpp"
#include <sstream>
#include <iostream>
#include <thread>
#include "logger.hpp"
#include <iostream>
#include <QFile>
#include <QTextStream>
#include <numeric>
#include <algorithm>

YakOsciloscope::YakOsciloscope(const std::string &deviceName):VisaOsciloscope(deviceName),divisions(3200)
{
    sendComand(":COMMUNICATE:HEADER OFF\n");
   // sendComand(":COMMUNICATE:REMOTE ON");
}

YakOsciloscope::~YakOsciloscope(){
   // sendComand(":COMMUNICATE:REMOTE OFF");

}

QString YakOsciloscope::chToString(const int ch)const{
    if(ch<=0 || ch > 4)
        throw std::runtime_error("Unknown chanel");
    else return tr("%1").arg(ch);
}

void YakOsciloscope::selectChanel(int chan){

    std::stringstream cmd;
    cmd << ":WAVEFORM:TRACE " << chToString(chan).toStdString() << ";:WAVEFORM:FORMAT WORD;START 0;END 25000\n";
    sendComand(cmd.str(),"Failed to set chanel");
    //std::this_thread::sleep_for(std::chrono::milliseconds(400));
}

std::tuple<long,float,float,float> YakOsciloscope::getChanelParams(){

    long dataLength=readValue<long>("WAVEFORM:LENGTH?\n");
    float range=readValue<float>("WAVEFORM:RANGE?\n");
    float offset=readValue<float>("WAVEFORM:OFFSET?\n");
    float div=1.0f*divisions;
    return std::make_tuple(dataLength,range,offset,div);
}

std::vector<double> YakOsciloscope::getCurve(std::tuple<long, float, float, float> &params){

    if(std::get<0>(params)<0)
        return std::vector<double>();

    sendComand("WAVEFORM:SEND?\n");

    ViStatus status;
    ViUInt32 length;
    char c;
    // The check char, Curve response is in the form:
    // #<0-9>{number of fields depending on previous value}{Data}
    // and we cannot skip so we read it

    status=viRead(vi,(ViBuf)&c,1,&length);
    checkStatus(status,"Failed reading #");
    if(c!='#')
        throw std::runtime_error("Inccorrect response, expected #");




    status=viRead(vi,(ViBuf)&c,1,&length);
    checkStatus(status,"Failed to scan start number size");
    if(c<'0' || c>'9')
        throw std::runtime_error("Incorrect start number size");

    //Read in data length;
    int count=c-'0';
    std::string len;
    for(int i=0;i<count;i++){
        status=viRead(vi,(ViBuf)&c,1,&length);
        len+=c;
        checkStatus(status,"Size reading failed");
    }

    std::stringstream conv(len);
    int dataL;
    conv >> dataL;
    if(dataL==0)
        throw std::runtime_error("Record size 0!");

    //Get actual data
    std::vector<short> resultData(dataL,0);
    status=viRead(vi,(ViBuf)resultData.data(),dataL,&length);
    checkStatus(status,"Curve data reading failed");

   // log() << "Read " << std::to_string(length) << " bytes\n";
    std::vector<double> mesResults(dataL/2);
    for(std::size_t i=0;i<mesResults.size();i++){

       mesResults[i]=(((double)resultData[i])*std::get<1>(params))/std::get<3>(params)+std::get<2>(params);

    }
    //std::this_thread::sleep_for(std::chrono::milliseconds(100));
    return mesResults;
}
double YakOsciloscope::getTimeStep(){
    return 1/readValue<float>(":TIMEBASE:SRATE?\n");
   }

void YakOsciloscope::setAverage(bool turnOn,int count){
    if(!turnOn)
        sendComand(":ACQUIRE:MODE NORMAL\n");
    else{
        std::stringstream buf;
        buf << ":ACQUIRE:MODE AVERAGE";
        if(count >0)
            buf << ";:ACQUIRE:AVERAGE:COUNT " << count;
        buf << "\n";
        sendComand(buf.str());
    }
}

QPair<bool,int> YakOsciloscope::getAverage(){

    bool isOn=readValue<std::string>(":ACQUIRE:MODE?\n")!="NORM\n";
    int avgNum=readValue<int>(":ACQUIRE:AVERAGE:COUNT?\n");

    return qMakePair(isOn,avgNum);
}

bool YakOsciloscope::getRunStop(){
    return  readValue<short>(":STATUS:CONDITION?\n")!=0;
}

void YakOsciloscope::setRunStop(bool run){
    if(run)
        sendComand(":START\n");
    else
        sendComand(":STOP\n");
}

void YakOsciloscope::SetTriggerChanel(int chan){
    sendComand(":TRIGGER:EDGE:SOURCE "+std::to_string(chan)+"\n");
}
void YakOsciloscope::SetTimeDiv(double timeDiv){

    sendComand(":TIMEBASE:TDIV "+std::to_string(timeDiv));
}

void YakOsciloscope::SetVOffset(int chan,double offset){
    sendComand(":CHANNEL"+std::to_string(chan)+":OFFSET "+std::to_string(offset)+"\n");
}

void YakOsciloscope::SetVDivision(int chan,double div){
    sendComand(":CHANNEL"+std::to_string(chan)+":VDIV "+std::to_string(div)+"\n");
}
void YakOsciloscope::SetVPosition(int chan, double pos){
    sendComand(":CHANNEL"+std::to_string(chan)+":POSITION "+std::to_string(pos)+"\n");
}

double YakOsciloscope::GetPosition(int chan ){
    return readValue<float>(":CHANNEL"+std::to_string(chan)+":POSITION?");
}
double YakOsciloscope::GetVOffset(int chan){
    return readValue<float>(":CHANNEL"+std::to_string(chan)+":OFFSET?");
}
double YakOsciloscope::GetVDivision(int chan){
    return readValue<float>(":CHANNEL"+std::to_string(chan)+":VDIV?");
}

void YakOsciloscope::ExpandChanel(int chan, double div, int ms, bool twoStep){

    try{
        selectChanel(chan);
        setAverage(true,128);
        SetVPosition(chan,0);
        SetVOffset(chan,0.0);
        SetVDivision(chan,1.0);
        setRunStop(true);
        std::this_thread::sleep_for(std::chrono::milliseconds(ms));
        setRunStop(false);

        auto curve=getCurve(getChanelParams());
        if(curve.size()==0){
            throw std::runtime_error("Unable to get curve, specify longer sleep time or check if chanel is on");
        }
        auto avg = std::accumulate(curve.begin(),curve.end(),0.0);
        avg=avg/curve.size();

//        std::this_thread::sleep_for(std::chrono::milliseconds(200));

        SetVOffset(chan,avg);

        if(twoStep){
            SetVDivision(chan,0.100);
            setRunStop(true);
            std::this_thread::sleep_for(std::chrono::milliseconds(ms));
            setRunStop(false);
            curve=getCurve(getChanelParams());
            avg = std::accumulate(curve.begin(),curve.end(),0.0);
            avg=avg/curve.size();
            SetVOffset(chan,avg);
        }

        SetVDivision(chan,div);
        setRunStop(true);
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    catch(std::exception & e){
        log() << "Expand chanel failed " << e.what() << "\n";
    }

}


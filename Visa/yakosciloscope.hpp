#ifndef YAKOSCILOSCOPE_HPP
#define YAKOSCILOSCOPE_HPP

#include "visaosciloscope.hpp"

class YakOsciloscope : public VisaOsciloscope
{
    const int divisions; // We use WORD output format
public:
    YakOsciloscope(const std::string & deviceName);

    virtual QString chToString(const int ch) const override;
    virtual void selectChanel(int chan) override;
    virtual std::tuple<long,float,float,float> getChanelParams() override;
    virtual std::vector<double> getCurve(std::tuple<long,float,float,float> & params) override;
    virtual double getTimeStep() override;
    virtual void setAverage(bool flag,int avg) override;
    virtual QPair<bool,int> getAverage() override;
    virtual bool getRunStop() override;
    virtual void setRunStop(bool flag)override;
    virtual void SetTriggerChanel(int chan) override;
    virtual void SetTimeDiv(double timeDiv) override;
    virtual void SetVOffset(int chan,double offset) override;
    virtual void SetVPosition(int chan, double pos) override;
    virtual void SetVDivision(int chan,double div) override;
    virtual double GetPosition(int chan) override;
    virtual double GetVOffset(int chan) override;
    virtual double GetVDivision(int chan) override;
    virtual void ExpandChanel(int chan, double div, int ms, bool twoStep) override;
    ~YakOsciloscope();

};

#endif // YAKOSCILOSCOPE_HPP

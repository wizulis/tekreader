#include <QPushButton>

#include "deviceobj.hpp"
#include "logger.hpp"
#include "Dialogs/devicewindow.hpp"


DeviceObj::DeviceObj(const QString & nm,const QString & par, load_t func):
          QObject(nullptr),name(nm),params(par),load(func),isLoaded(false),button(nullptr),obj(nullptr){

}

DeviceObj::DeviceObj(const DeviceObj &obj):
    QObject(nullptr),name(obj.name),params(obj.params),load(obj.load),isLoaded(obj.isLoaded),button(obj.button),obj(obj.obj){

}


void DeviceObj::closeDevice(){
   if(!isLoaded)
        return;
    isLoaded=false;
    obj->deleteLater();
    button->setText(tr("Ielādēt"));
}

void DeviceObj::toggleDevice(){
    if(!isLoaded){
        try{
           obj=load(this,params);
           button->setText(tr("Izlādēt"));
           isLoaded=true;
        }catch(std::exception & e){
            log() << "Error loading device: " <<  e.what();

        }
    }else{
        closeDevice();
    }
}

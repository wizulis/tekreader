#ifndef DEVICEOBJ_HPP
#define DEVICEOBJ_HPP

#include <QObject>
#include <functional>
class DeviceWindow;
class QPushButton;

class DeviceObj:public QObject{

    Q_OBJECT
public:
    typedef std::function<DeviceWindow*(DeviceObj *, const QString &)> load_t;

    DeviceObj(const QString & nm,const QString & par,load_t func);
    DeviceObj(const DeviceObj &);

    QString name;
    QString params;
    load_t load;
    bool isLoaded;
    QPushButton * button;
    DeviceWindow * obj;
public slots:
    void toggleDevice();
    void closeDevice();
};

#endif // DEVICEOBJ_HPP

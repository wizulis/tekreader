#include "filewriter.hpp"

FileWriter::FileWriter(QObject *parent) :
    QObject(parent)
{
}


bool FileWriter::openFile(const QString &name, bool append, bool clear){

    QIODevice::OpenMode mode=QIODevice::Text | QIODevice::ReadWrite;

    if(append)
        mode|=QIODevice::Append;
    else if(clear)
        mode|=QIODevice::Truncate;

    close();

    file.setFileName(name);
    if(!file.open(mode))
        throw std::runtime_error(file.errorString().toStdString());
    return true;
}

void FileWriter::write(const QString &data){
    if(!file.isOpen()){
        throw std::runtime_error("File Not Open");
    }
    file.write(data.toStdString().c_str());
}

QString FileWriter::readLine(){
    if(!file.isOpen()){
        throw std::runtime_error("File Not Open");
    }
    return file.readLine();
}

void FileWriter::flush(){
    if(file.isOpen())
        file.flush();
}

void FileWriter::close(){
    if(!file.isOpen())
        file.close();

}

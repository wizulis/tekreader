#ifndef FILEWRITER_HPP
#define FILEWRITER_HPP

#include <QObject>
#include <QString>
#include <QFile>
#include <QScriptable>
#include <QScriptValue>
#include <QScriptEngine>
class FileWriter : public QObject
{
    QFile file;
    Q_OBJECT
public:
    explicit FileWriter(QObject *parent = 0);
    QScriptValue qscript_call(QWidget *parent = 0);

signals:

public slots:
    bool openFile(const QString & name,bool append,bool clear);
    void write(const QString & name);
    QString readLine();
    void flush();
    void close();
};

class FileWriter_Wrap:public FileWriter,protected QScriptable{
    Q_OBJECT
public:
    FileWriter_Wrap(QWidget * parent=nullptr):FileWriter((QObject*)parent){}
public slots:

    QScriptValue qscript_call(QWidget *parent=nullptr){
        FileWriter * const iface=new FileWriter_Wrap(parent);
        return engine()->newQObject(iface, QScriptEngine::AutoOwnership);
    }
};

#endif // FILEWRITER_HPP

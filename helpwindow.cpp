#include "helpwindow.hpp"
#include <QHBoxLayout>
#include <QHelpContentWidget>
HelpWindow::HelpWindow(QWidget *parent) :
    QDialog(parent)
{
    resize(500,500);
    setModal(false);
    auto lay= new QHBoxLayout(this);

    splt= new QSplitter(Qt::Horizontal,this);
    lay->addWidget(splt);

}


void HelpWindow::loadResource(const QString &str){

    eng = new QHelpEngine(str,this);
    eng->setupData();
    splt->addWidget(eng->contentWidget());
    auto * helpView = new HelpWidget(eng,this);
    splt->addWidget(helpView);
    connect(eng->contentWidget(),&QHelpContentWidget::linkActivated,helpView,&HelpWidget::setResource);
}

HelpWidget::HelpWidget(QHelpEngine *helpObj, QWidget *parent):QTextEdit(parent),eng(helpObj){

}

void HelpWidget::setResource(const QUrl &url){

    QString data=eng->fileData(url);
    document()->setHtml(data);
}

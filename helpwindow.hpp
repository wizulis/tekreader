#ifndef HELPWINDOW_HPP
#define HELPWINDOW_HPP

#include <QDialog>
#include <QHelpEngine>
#include <QSplitter>
#include <QTextEdit>
class HelpWindow : public QDialog
{
    Q_OBJECT

    QHelpEngine * eng;
    QSplitter * splt;

public:
    explicit HelpWindow(QWidget *parent = 0);

signals:

public slots:
    void loadResource(const QString & str);
};

class HelpWidget: public QTextEdit{

    Q_OBJECT

    QHelpEngine * eng;
public:
    HelpWidget(QHelpEngine * helpObj,QWidget * parent=0);

public slots:
    void setResource(const QUrl & url);
};

#endif // HELPWINDOW_HPP

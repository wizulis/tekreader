#ifndef INDICATORLIGHT_HPP
#define INDICATORLIGHT_HPP

#include <QWidget>
#include <QPainter>
#include <functional>

class IndicatorLight : public QWidget
{
    Q_OBJECT

    int state;
    std::function<QColor(int)> stateFunction;
public:
    explicit IndicatorLight(std::function<QColor(int)> callBack,QWidget *parent = 0):
        QWidget(parent),stateFunction(callBack){

    }

    int getState(){return state;}


    QSize sizeHint()const override {return QSize(10,10);}
protected:
    virtual void paintEvent(QPaintEvent *)override{
        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing);
        painter.setBrush(stateFunction(state));

        painter.drawEllipse(0,0,10,10);
    }

signals:


public slots:
    void setState(int st){
        state=st;
        repaint();
    }

};

#endif // INDICATORLIGHT_HPP

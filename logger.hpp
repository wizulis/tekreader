#ifndef LOGGER_HPP
#define LOGGER_HPP
#include <string>
#include <functional>
#include <iostream>

class Logger
{
    friend Logger & log();

    Logger(){
      //  callback=[](const std::string & outp){std::cout << outp << std::flush;};
    }

    static std::function<void(const std::string&)> callback;
public:
    void setCallback(std::function<void(const std::string &)> & cb){
        callback=cb;
    }

    void write(const std::string & outp){
        if(callback)
            callback(outp);
    }

    Logger & operator << (const std::string & obj){
        write(obj);
        return *this;
    }
    Logger & operator << (const int obj){
        write(std::to_string(obj));
        return *this;
    }

    Logger & operator << (const double obj){
        write(std::to_string(obj));
        return *this;
    }
};



static Logger & log(){
    static Logger ret;
    return ret;
}

#endif // LOGGER_HPP

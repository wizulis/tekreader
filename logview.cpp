#include "logview.hpp"
#include <QTextEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QTextCursor>
#include <QTimer>
LogView::LogView(QWidget *parent) :
    QDialog(parent)
{
    setModal(false);
    resize(400,600);
    auto * vbox = new QVBoxLayout(this);

    auto * hbox=new QHBoxLayout();
    hbox->addStretch(1);
    auto * clBut=new QPushButton(tr("Notīrīt"),this);
    hbox->addWidget(clBut);
    vbox->addLayout(hbox);
    vbox->addWidget(tEdit=new QTextEdit(this));

    auto * timer = new QTimer(this);
    timer->setInterval(200);
    timer->start();
    connect(clBut,&QPushButton::clicked,tEdit,&QTextEdit::clear);
    connect(timer,&QTimer::timeout,this,&LogView::updateLog);
}

void LogView::log(const std::string &str){
    QMutexLocker lock(&bufferMutex);
    buffer.append(QString::fromStdString(str));

}

void LogView::updateLog(){
    QMutexLocker lock(&bufferMutex);

    if(buffer.size()==0)
        return;

    QTextCursor cursor(tEdit->document());
    cursor.movePosition(QTextCursor::End);
    for(const QString & str:buffer)
        cursor.insertText(str);
    buffer.clear();

}

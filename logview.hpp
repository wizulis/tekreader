#ifndef LOGVIEW_HPP
#define LOGVIEW_HPP

#include <QDialog>
#include <QList>
#include <QString>

class QTextEdit;
class LogView : public QDialog
{
    Q_OBJECT
    mutable QMutex bufferMutex;
    QList<QString> buffer;
    QTextEdit * tEdit;

public:
    explicit LogView(QWidget *parent = 0);

signals:

public slots:
    void log(const std::string & str);
    void updateLog();
};

#endif // LOGVIEW_HPP



function createColumn(func,masivs)
     local ret=dVec(masivs:Size())
        for i = 0,masivs:Size()-1,1 do
                local v=masivs:Get(i)
                ret:Set(i, func(v))
        end
     return ret
end

function selectChanels(chan)
    local ret=strVec(#chan)
    for i,v in ipairs(chan) do
        ret:Set(i-1,v)
    end
    return ret
end

function appendColumn(res,newCol,xCol,func)
    col=res:GetColumn(xCol)
    nCol=createColumn(func,col)
    res:AddColumn(newCol,nCol)
end

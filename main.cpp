#include <QApplication>
#include <functional>

#include "mainwindow.hpp"
#include "logger.hpp"
#include "logview.hpp"
#include "helpwindow.hpp"
#include <boost/python.hpp>
#include <QShortcut>
#include<QKeySequence>
#include <Boost/python/suite/indexing/vector_indexing_suite.hpp>


std::function<void(const std::string&)> Logger::callback;

int main(int argc, char *argv[])
{

    QApplication app(argc,argv);
    QApplication::setOrganizationName("Lazercentrs");
    QApplication::setOrganizationDomain("LU.LV");
    QApplication::setApplicationName("TekReader");


    MainWindow win;

    auto & tmpLog=log();
    auto * tmpLogView= new LogView(&win);

    QShortcut shrtC(QKeySequence(Qt::Key_F12),&win);
    shrtC.setContext(Qt::ApplicationShortcut);
    QObject::connect(&shrtC,&QShortcut::activated,[tmpLogView](){tmpLogView->setVisible(!tmpLogView->isVisible());});
    //std::function<void(const std::string &)> tmpCb(std::bind(&LogView::log,tmpLogView,std::placeholders::_1));
    std::function<void(const std::string &)> tmpCb=[=](const std::string &str)->void{qDebug() << str.c_str(); tmpLogView->log(str);};
    tmpLog.setCallback(tmpCb);

    win.show();


    int ret= app.exec();
    return ret;
}

#include "mainwindow.hpp"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>

#include <QFileDialog>
#include <QMessageBox>
#include <QLabel>
#include <QString>
#include <QFileInfo>
#include <QStatusBar>
#include <iostream>
#include "logger.hpp"

#include "plotviewwidget.hpp"
#include "luascriptwidget.hpp"
#include "Activex/pjezocontroler.h"
#include "Dialogs/osciloscopewindow.hpp"
#include "Dialogs/scanner.hpp"
#include "Dialogs/devicewindow.hpp"
#include "yakosciloscope.hpp"
#include "agilentosciloscope.hpp"
#include <QDebug>


DeviceWindow * loadTek(DeviceObj * parent,QString params){
    qDebug() << "Load Tek";
    QSharedPointer<VisaOsciloscope> osc = QSharedPointer<VisaOsciloscope>( new TekOsciloscope(params.toStdString()));
    auto * oscW= new OsciloscopeWindow(osc,parent);
    oscW->show();
    return oscW;
}

DeviceWindow * loadYak(DeviceObj * parent,QString params){
    qDebug() << "Load Yak";
    QSharedPointer<VisaOsciloscope> osc = QSharedPointer<VisaOsciloscope>( new YakOsciloscope(params.toStdString()));
    auto * oscW= new OsciloscopeWindow(osc,parent);
    oscW->show();
    return oscW;
}
DeviceWindow * loadAgilent(DeviceObj * parent,QString params){
    qDebug() << "Load Agilent";
    QSharedPointer<VisaOsciloscope> osc = QSharedPointer<VisaOsciloscope>( new AgilentOsciloscope(params.toStdString()));
    auto * oscW= new OsciloscopeWindow(osc,parent);
    oscW->show();
    return oscW;
}

DeviceWindow * loadScanner(DeviceObj * parent,QString params){
    auto * sc = new Scanner(parent);
    sc->show();
    return sc;
}

DeviceWindow * loadSignalGen(DeviceObj * parent,QString params){
    auto * sc = new SignalGen(parent,params);
    sc->show();
    return sc;
}
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    this->setFixedSize(450,400);
    QWidget * central=new QWidget();
    setCentralWidget(central);

    auto vbox1=new QVBoxLayout(central);

    itemTable =new QTableWidget(central);
    itemTable->setColumnCount(3);
    itemTable->setHorizontalHeaderLabels(QStringList() << tr("Ierīce") << tr("Identifikators") << tr("Ielāde"));
    itemTable->setColumnWidth(0,110);
    itemTable->setColumnWidth(1,200);
    vbox1->addWidget(itemTable);


    registerDevice("Tektronix","USB::0x0699::0x0365::C030126::INSTR",loadTek);
    registerDevice("Yakogowa-NET","TCPIP::5.179.18.184::INSTR",loadYak);
    registerDevice("Agilent-NET","TCPIP::5.179.18.36::INSTR",loadAgilent);
    registerDevice("Scanner","",loadScanner);
    registerDevice("TTSignalGen","ASRL9::INSTR",loadSignalGen);

}
int MainWindow::registerDevice(const QString &name, const QString &paramString, DeviceObj::load_t loadFunction){
    int row=devices.size();

    devices.push_back(DeviceObj(name,paramString,loadFunction));
    itemTable->insertRow(row);
    itemTable->setCellWidget(row,0,new QLabel(name));
    itemTable->setCellWidget(row,1,new QLineEdit(paramString));
    auto * but=new QPushButton(tr("Ielādēt"));
    itemTable->setCellWidget(row,2,but);
    devices.back().button=but;

    connect(but,&QPushButton::clicked,&(devices.back()),&DeviceObj::toggleDevice);
    return row;
}

void MainWindow::loadSettings(){

    QSettings settings;
    move(settings.value("position",QPoint(100,100)).toPoint());
}

void MainWindow::saveSettings(){
    QSettings settings;
    settings.setValue("position",pos());
}

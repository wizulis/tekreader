#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <memory>
#include <QString>
#include <QMainWindow>
#include <QLineEdit>
#include <QSettings>
#include <QTabWidget>
#include <QComboBox>
#include <functional>
#include <QTableWidget>
#include <QCoreApplication>
#include "deviceobj.hpp"


#include "qcustomplot.h"

#include "visaosciloscope.hpp"
#include "luascriptwidget.hpp"
#include <QDebug>


class PlotViewWidget;
class QPushButton;


class MainWindow : public QMainWindow
{
    Q_OBJECT


    QTableWidget * itemTable;

    std::list<DeviceObj> devices;

    void loadSettings();
    void saveSettings();

public:
    explicit MainWindow(QWidget *parent = 0);

    int registerDevice(const QString & name,const QString & paramString,DeviceObj::load_t loadFunction);

    ~MainWindow(){
        saveSettings();
    }
    virtual void closeEvent(QCloseEvent * event){
        event->accept();
        QCoreApplication::quit();
    }
};

#endif // MAINWINDOW_HPP

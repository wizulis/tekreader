#include "positionsocket.hpp"
#include <sstream>
#include "logger.hpp"
PositionSocket::PositionSocket(std::string  p):pipeName(p){

}

#include <thread>

void PositionSocket::SetPositionC(float pos,int count){
    if(count <0){
        log() << "ERROR " << std::to_string(GetLastError());
        throw std::runtime_error("Write failed");
    }

    std::wstringstream wstr;
    wstr << L"\\\\.\\pipe\\" << pipeName.c_str();
    pipe=CreateFile(wstr.str().c_str(),
                    GENERIC_READ|GENERIC_WRITE,
                    FILE_SHARE_READ|FILE_SHARE_WRITE,
                    NULL,
                    OPEN_EXISTING,
                    FILE_ATTRIBUTE_NORMAL,
                    NULL);
    if(pipe==INVALID_HANDLE_VALUE)
        throw std::runtime_error("INVALID HANDLE");

    std::string pos_str=std::to_string(pos);
    DWORD bytes_writen;
    bool res=WriteFile(pipe,pos_str.c_str(),pos_str.size(),&bytes_writen,0);
    CloseHandle(pipe);
    if(!res){
        log() << "Retrying set position\n";
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        SetPositionC(pos,count-1);
    }
}
void PositionSocket::SetPosition(float pos){
    SetPositionC(pos,4);
}

void PositionSocket::Close(){
    std::wstringstream wstr;
    wstr << L"\\\\.\\pipe\\" << pipeName.c_str();
    pipe=CreateFile(wstr.str().c_str(),
                    GENERIC_READ|GENERIC_WRITE,
                    FILE_SHARE_READ|FILE_SHARE_WRITE,
                    NULL,
                    OPEN_EXISTING,
                    FILE_ATTRIBUTE_NORMAL,
                    NULL);
    if(pipe==INVALID_HANDLE_VALUE)
        throw std::runtime_error("INVALID HANDLE");

    std::string cmd="Close";
    DWORD bytes_writen;
    bool res=WriteFile(pipe,cmd.c_str(),cmd.size(),&bytes_writen,0);
    if(!res){
        log() << "ERROR " << std::to_string(GetLastError());
        CloseHandle(pipe);
        throw std::runtime_error("Write failed");
    }
    CloseHandle(pipe);
}

PositionSocket::~PositionSocket(){


}

#ifndef POSITIONSOCKET_HPP
#define POSITIONSOCKET_HPP
#include "Windows.h"
#include <string>
class PositionSocket
{

    HANDLE pipe;
    std::string pipeName;
    int serial,chanel;

public:
    PositionSocket(std::string pipeName);

    void SetPositionC(float pos,int count);
    void SetPosition(float pos);
    void Close();
    ~PositionSocket();
};

#endif // POSITIONSOCKET_HPP

#include "scanner2d.hpp"
#include <QDebug>
#include <QVBoxLayout>
#include "Dialogs/osciloscopewindow.hpp"
#include "Dialogs/signalgen.hpp"

#include <numeric>


Scanner2D::Scanner2D(QWidget *parent) :
    QWidget(parent),reverse(false)
{
    is_started=false;

    posX=0;
    posY=4;
    step=0.1;
    stepTime=1500;
    QWidget *wx= new QWidget(this),*wy=new QWidget(this);
    motorX= new MotorControler(wx);
    motorY= new MotorControler(wy);
    motorX->startControl();
    motorY->startControl();

    QVBoxLayout * vbox= new QVBoxLayout(this);
    vbox->addWidget(wx);
    vbox->addWidget(wy);
    timer=new QTimer(this);
    connect(timer,&QTimer::timeout,this,&Scanner2D::iterate);

    motorX->setSerial(90841836);
    motorY->setSerial(90841837);
    connect(motorX,&MotorControler::MoveComplete,this,&Scanner2D::OnMoveCompleted);
  }
#include <iostream>

void Scanner2D::OnMoveCompleted(int){
    if(save){
        auto * osc = OsciloscopeWindow::getLastLoaded();
        if(osc!=nullptr)
            osc->SetRunStop(true);
    }
    if(is_started)
        timer->start();
}
//-0.5,-0.3,-0.1,0,0.1,0.3,0.5
void Scanner2D::saveAvg(OsciloscopeWindow * osc,double pX,double pY)const{
    QString name=osc->GetSaveName();
    std::fstream out;
    out.open(name.toStdString(),std::ios::app);
    if(!out.is_open())
        return;

    out << posX <<"\t" << posY << "\t";
    if(mag_ind<(int)magnetic_values.size()){
        out << magnetic_values[mag_ind];
    }else{
        out << "-";
    }

    out <<"\t";

    auto col=osc->getOsc()->getLastData()->getColumn("CH1");
    double avg=0; double err=0;

    avg=std::accumulate(col.begin(),col.end(),avg)/col.size();
    err=std::accumulate(col.begin(),col.end(),err,[avg](double x,double y){return x+pow(avg-y,2);});
    err=std::sqrt(err/col.size()/(col.size()-1));

    out << avg << "\t" << err << "\n";
}

void Scanner2D::saveDistr(OsciloscopeWindow * osc,double pX,double pY)const{

    QString name=osc->GetSaveName();
    std::fstream out;
    out.open(name.toStdString(),std::ios::app);
    if(!out.is_open())
        return;
    auto ch1=osc->getOsc()->getLastData()->getColumn("CH1");
    auto ch2=osc->getOsc()->getLastData()->getColumn("CH2");

    int idx=0;
    double avg1=0;
    double avg2=0;
    for(int i=0;i<ch1.size();i++){
        avg1+=ch1[i];
        avg2+=ch2[i];
        idx++;
        if(idx==20){
            avg1/=idx;
            avg2/=idx;
            out << pX <<"\t" << pY << "\t" << avg2 << "\t" << avg1 << "\n";
            avg1=avg2=0;
            idx=0;
        }
       // out << pX << "\t" << pY << "\t" << ch2[i] << "\t" << ch1[i] << "\n";
    }


    if(idx!=0){
        avg1/=idx;
        avg2/=idx;
        out << pX <<"\t" << pY << "\t" << avg2 << "\t" << avg1 << "\n";
    }

}

void Scanner2D::iterate(){
    if(save){
        auto * osc=OsciloscopeWindow::getLastLoaded();
        if(osc!=nullptr){
            osc->SetRunStop(false);
            osc->Mesure();
            if(magnetic_values.size()==0){
                saveDistr(osc,posX,posY);
            }
            else{
                saveAvg(osc,posX,posY);
            }

        }
    }
    mag_ind++;
    if(mag_ind<(int)magnetic_values.size()){
        auto * gen =SignalGen::getCurrentGenerator();
        if(gen!=nullptr){
            gen->setOffset(magnetic_values[mag_ind]);
            gen->sendOffset();
        }

        auto * osc=OsciloscopeWindow::getLastLoaded();
        if(osc!=nullptr){
            osc->SetRunStop(true);
        }
        return;
    }else{
        mag_ind=0;
        if(magnetic_values.size()>0){
             auto * gen =SignalGen::getCurrentGenerator();
            if(gen!=nullptr){
                gen->setOffset(magnetic_values[mag_ind]);
                gen->sendOffset();
            }
        }
    }

    if(!reverse){
        posX+=step;
        posY-=step;
        if(posX>=4 || posY<=0){
            reverse=true;
            posX=4;
            posY=0;
            id++;
        }

    }else{
        posX-=step;
        posY+=step;
        if(posX<=0 || posY>=4){
            reverse=false;
            posX=0;
            posY=4;
            id++;
        }
    }
    motorX->setPosition(0,posX);
    motorY->setPosition(0,posY);
    timer->stop();
}

void Scanner2D::start(){
    posX=motorX->getPosition();
    posY=motorY->getPosition();

    if(timer->isActive()){
        timer->stop();
    }
    save=false;
    is_started=true;
    timer->start(stepTime);
}
void Scanner2D::start_spec(std::vector<double> vals){

    if(timer->isActive()){
        timer->stop();
    }
    mag_ind=0;
    id=0;

    posX=motorX->getPosition();
    posY=motorY->getPosition();

    save=true;
    magnetic_values=vals;
    if(magnetic_values.size()>0){
        auto * gen=        SignalGen::getCurrentGenerator();
        if(gen!=nullptr){
            gen->setOffset(magnetic_values[0]);
            gen->sendOffset();
        }
    }
    is_started=true;
    timer->start(stepTime);
}

void Scanner2D::stop(){
    save=false;
    is_started=false;
    if(timer->isActive()){
        timer->stop();
    }
}

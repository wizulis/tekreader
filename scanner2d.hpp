#ifndef SCANNER2D_HPP
#define SCANNER2D_HPP

#include <QWidget>
#include "Activex/motorcontroler.hpp"
#include <QTimer>
#include <QDebug>
class OsciloscopeWindow;
class Scanner2D : public QWidget
{
    Q_OBJECT


    bool reverse;
    double posX,posY;
    double step;
    int stepTime;
    MotorControler *motorX,*motorY;
    QTimer * timer;
    bool save;
    bool is_started;
    int id;
    std::vector<double> magnetic_values;
    int mag_ind;
public:
    explicit Scanner2D(QWidget *parent = 0);

    void setStep(double newStep){step=newStep;if(timer->isActive())start();}
    double getStep(){return step;}

    void setStepTime(int newTime){
        stepTime=newTime;if(timer->isActive())start();
    }
    int getStepTime(){
        return stepTime;
    }


    ~Scanner2D(){
        stop();
    }

    bool isActive(){
        return timer->isActive();
    }
    void saveAvg(OsciloscopeWindow * osc,double pX,double pY)const;
    void saveDistr(OsciloscopeWindow * osc,double pX,double pY)const;
signals:

public slots:
    void OnMoveCompleted(int);
    void iterate();
    void start();
    void start_spec(std::vector<double> vals);
    void stop();

};

#endif // SCANNER2D_HPP
